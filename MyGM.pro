QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
unix|win32: LIBS += -lGL -lsfml-graphics -lsfml-window -lsfml-system  -lsfml-network

SOURCES += \
    GameObject/Interface/Widget/CheckBox.cpp \
    GameObject/Scene/sceneGame.cpp \
    GameObject/Scene/sceneLogin.cpp \
        main.cpp \
    GameObject/TypeObject/balllightning.cpp \
    GameObject/Weapon/weapon.cpp \
    GameObject/gameobject.cpp \
    GameObject/player.cpp \
    GameObject/Weapon/lightning.cpp \
    GameObject/npc.cpp \
    GameObject/TypeObject/human.cpp \
    GameObject/Interface/Widget/Button.cpp \
    GameObject/Interface/Widget/Widget.cpp \
    GameObject/Interface/Interface.cpp \
    GameObject/Interface/Widget/Text.cpp \
    GameObject/StaticObject/staticobject.cpp \
    GameObject/StaticObject/bush.cpp \
    GameObject/TypeObject/typeobject.cpp \
    GameObject/Scene/scenemovetest.cpp \
    game.cpp \
    GameObject/Scene/scenetestchecknpc.cpp \
    GameObject/Interface/Widget/Layout.cpp \
    GameObject/Scene/scenetestlayout.cpp \
    GameObject/Interface/Widget/TextEdit.cpp \
    GameObject/Scene/scenetestedit.cpp \
    GameObject/Interface/Widget/ScrollLayout.cpp \
    GameObject/Interface/Widget/ComboBox.cpp \
    GameObject/Scene/SceneTestGrid.cpp \
    GameObject/Scene/sceneTestServerOrClient.cpp \
    clientTCP.cpp \
    serverTCP.cpp \
    clientUDP.cpp \
    serverUDP.cpp \
    GameManager.cpp \
    GameObject/Scene/StartScene.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    GameObject/Interface/Widget/CheckBox.h \
    GameObject/TypeObject/balllightning.h \
    GameObject/Weapon/weapon.h \
    GameObject/gameobject.h \
    GameObject/player.h \
    GameObject/Weapon/lightning.h \
    GameObject/npc.h \
    GameObject/TypeObject/human.h \
    GameObject/Interface/Widget/Button.h \
    GameObject/Interface/Widget/Widget.h \
    GameObject/Interface/Interface.h \
    GameObject/Interface/Widget/Text.h \
    GameObject/StaticObject/staticobject.h \
    GameObject/StaticObject/bush.h \
    GameObject/TypeObject/typeobject.h \
    variable_storage.h \
    game.h \
    GameObject/Interface/Widget/Layout.h \
    GameObject/Interface/Widget/TextEdit.h \
    GameObject/Interface/Widget/ScrollLayout.h \
    GameObject/Interface/Widget/ComboBox.h \
    lib/microbson.hpp \
    lib/minibson.hpp \
    GameManager.h \
    GameObject/Scene/Scene.h
