#include "game.h"
static Game::ServerTCP* server = nullptr;

Game::ServerTCP::ServerTCP(unsigned short p)
{
    port = p;
    threadWaitConnect = new sf::Thread(&ServerTCP::waitConnectNewSocket,this);

    if(server == nullptr){
        server = this;
    }
}

void Game::ServerTCP::waitNewSocket()
{
    if(!flagWaitConnect){

        flagWaitConnect = true;
        threadWaitConnect->launch();
    }
}

void Game::ServerTCP::waitConnectNewSocket()
{
    std::cout << "waitNewSocket" << std::endl;
    if (listener.listen(port) != sf::Socket::Status::Done)
    {
        std::cout << "Not ServerStart" << std::endl;
        return;
    }
    while (flagWaitConnect) {
        Socket* _socked = new Socket();
        if(listener.accept(_socked->socket) != sf::Socket::Socket::Done){
            std::cout << "Not ServerStart" << std::endl;
            return;
        }
        std::cout << "ServerStart" << std::endl;

        sf::Packet packetSend;
        packetSend << "get" << "login";
        _socked->socket.send(packetSend);

        sf::Packet packet;
        if(_socked->socket.receive(packet) == sf::Socket::Status::Done) {
            std::string login;
            if(packet >> login){
                _socked->login = login;
                _socked->threadReseive = new sf::Thread(&ServerTCP::receiveMessage, this);
                _socked->threadReseive->launch();
                sockets.push_back(_socked);
                return;
            }
        }
        std::cout << "Server new connection success login = ["<< _socked->login  << "]" << std::endl;
    }
}

void Game::ServerTCP::receiveMessage()
{
    Socket* socket = sockets[sockets.size()-1];
    socket->flagReseive= true;
    while (socket->flagReseive) {
        sf::Packet packet;
        sf::Socket::Status status = socket->socket.receive(packet);
        if(status == sf::Socket::Status::Done) {
            mutex.lock();
            std::string cmd;
            std::string data;
            if(packet >> cmd >> data){
                if(cmd == "playerUpdate")
                {
                    if (playerUpdate!=nullptr) {
                        playerUpdate(data);
                    }
                    continue;
                }
                else if(cmd == "bson")
                {
                    minibson::document doc(data.c_str(),data.size());
                    std::cout << doc.get("cmd","")<< std::endl;
                    std::cout << doc.get("Nomer",0)<< std::endl;
                    std::cout << static_cast<float>(doc.get("float",0.0))<< std::endl;
                    std::cout << doc.get("bool",false)<< std::endl;
                    continue;
                }
                else if(data == "Hi")
                {
                    send(cmd,"how are you");
                }
                std::cout << "Server::receiveMessage login ["<< socket->login <<  "] cmd =[" << cmd <<"] " << " data =[" <<data <<"]" << std::endl;
            }
            mutex.unlock();
        }
        else if(status == sf::Socket::Status::Disconnected)
        {
            socket->socket.disconnect();
            socket->flagReseive = false;
            std::cout << "ServerTCP::receive Disconect" << std::endl;
//            return;
        }
        else if(status == sf::Socket::Status::NotReady)
        {
            std::cout << "ServerTCP::receive NotReady" << std::endl;
        }
        else if(status == sf::Socket::Status::Partial)
        {
            std::cout << "ServerTCP::receive Partial" << std::endl;
        }
    }
}

void Game::ServerTCP::disconnect(std::string n)
{
    for (auto it = sockets.begin(); it != sockets.end(); it++)
    {
        if ((*it)->login == n) {
            (*it)->socket.disconnect();
            (*it)->flagReseive = false;
            break;
        }
    }

}

void Game::ServerTCP::send(std::string n,std::string s)
{
    sf::Packet packet;
    packet << s << "";
    for (auto it = sockets.begin(); it != sockets.end(); it++)
    {
        if ((*it)->login == n) {
            sf::Socket::Status status = (*it)->socket.send(packet);
            if (status == sf::Socket::Status::Error) {
                std::cout << "ServerTCP::send Error" << std::endl;
            }
            else if(status == sf::Socket::Status::Disconnected)
            {
                std::cout << "ServerTCP::send Disconect" << std::endl;
                return;
            }
            else if(status == sf::Socket::Status::NotReady)
            {
                std::cout << "ServerTCP::send NotReady" << std::endl;
            }
            else if(status == sf::Socket::Status::Partial)
            {
                std::cout << "ServerTCP::send Partial" << std::endl;
            }
            break;
        }
    }
}

void Game::ServerTCP::send(Socket* socket,std::string s)
{
    sf::Packet packet;
    packet << s << "";
    sf::Socket::Status status = socket->socket.send(packet);
    if (status == sf::Socket::Status::Error) {
        std::cout << "ServerTCP::send Error" << std::endl;
    }
    else if(status == sf::Socket::Status::Disconnected)
    {
        std::cout << "ServerTCP::send Disconect" << std::endl;
        return;
    }
    else if(status == sf::Socket::Status::NotReady)
    {
        std::cout << "ServerTCP::send NotReady" << std::endl;
    }
    else if(status == sf::Socket::Status::Partial)
    {
        std::cout << "ServerTCP::send Partial" << std::endl;
    }
}

void Game::ServerTCP::send(minibson::document doc)
{
    sf::Packet packet;
    packet << "update" << doc.getString();
    for (auto it = sockets.begin(); it != sockets.end(); it++)
    {
        sf::Socket::Status status = (*it)->socket.send(packet);
        if (status == sf::Socket::Status::Error) {
            std::cout << "ServerTCP::send Error" << std::endl;
        }
        else if(status == sf::Socket::Status::Disconnected)
        {
            std::cout << "ServerTCP::send Disconect" << std::endl;
            return;
        }
        else if(status == sf::Socket::Status::NotReady)
        {
            std::cout << "ServerTCP::send NotReady" << std::endl;
        }
        else if(status == sf::Socket::Status::Partial)
        {
            std::cout << "ServerTCP::send Partial" << std::endl;
        }
    }
}

std::vector<std::string> Game::ServerTCP::getActiveConnect()
{
    std::vector<std::string> list;
    for (auto it = sockets.begin(); it != sockets.end(); it++)
    {
        list.push_back((*it)->login);
    }
    return list;
}


Game::ServerTCP* Game::getServerTCP(){
    return server;
}

////////////////////////////удаление косячит
void Game::ServerTCP::serverStop()
{
    flagWaitConnect = false;
    listener.close();
    threadWaitConnect->wait();
    for (auto it = sockets.begin();it!= sockets.end();it++) {
        (*it)->flagReseive = false;
        (*it)->socket.disconnect();
#ifdef __linux
        (*it)->threadReseive->terminate();
#else
        (*it)->threadReseive->wait();
#endif
        delete *it;
    }
//    delete &listener;
    std::cout << "ServerTCP disconect serverStop" << std::endl;
}
