#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include "GameObject/player.h"

namespace Game {
    class GameManager
    {
    public:
        GameManager();
        static GameManager *geThis();
        void startGame();
        Game::Player player;

        void (*exit)() = nullptr;
        void (*start)() = nullptr;
        void (*pause)() = nullptr;
    private:
        void updateServer();
        sf::Thread* threadServerUpdate = nullptr;
        void updateClient();
        sf::Thread* threadClientUpdate = nullptr;
    };
}

#endif // GAMEMANAGER_H
