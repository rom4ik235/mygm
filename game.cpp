#include "game.h"

void Game::playerUpdate(std::string data)
{
    minibson::document doc(data.c_str(),data.size());
    if(doc.get("cmd","")=="update"){
        sf::Mutex m;
        m.lock();
        (*Game::GameObjects::getDinamicObjects())[doc.get("index",0)]->update(doc.get("doc",minibson::document()));
        m.unlock();
    }
}
