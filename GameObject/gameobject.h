#ifndef GAMEOBJECT1__H
#define GAMEOBJECT1__H

#ifdef __linux__
#include "variable_storage.h"
#include "GameObject/Interface/Interface.h"
#include "GameObject/TypeObject/typeobject.h"
#else
#include "D:/WorkSpace/mygm/GameObject/TypeObject/typeobject.h"
#include "D:/WorkSpace/mygm/variable_storage.h"
#include "Interface/Interface.h"
#endif
namespace Game {
	namespace GameObjects {

		enum AnimationEnum {
			Inaction,
			Move,
			MoveAndAttack,
			Attack,
			ReceivedDamage,
			Death
		};

		class DinamicObject {
		public:
			
			virtual ~DinamicObject();
			
			virtual bool update(minibson::document property);
			
			virtual minibson::document update();
			
			virtual void draw(sf::RenderWindow* _window);
			
			virtual void contactObject(minibson::document property);
			
			virtual bool checkContactObject(long int i);
			
			float getRadiusAreaHit();
			
			sf::FloatRect getRect();
			
			sf::Vector2f getPos();
			
			const char* nameObj = "";
		protected:
			void death();
			sf::RectangleShape rectLocation;
			sf::CircleShape areaHit;
			sf::Vector2f speed;
			AnimationEnum anim = AnimationEnum::Inaction;
			bool removeObject = false;
		};

		std::map<long, Game::GameObjects::DinamicObject*>* getDinamicObjects();
		std::map<long, sf::FloatRect>* getArrayRectLocation();
		sf::RenderWindow* getWindow();

		void setWindow(sf::RenderWindow* W);

		void CheckContact();

		template<class T>
		long int addObjectToListMap(T obj) {
			for (long int i = 0; true; i++) {
				if ((*getDinamicObjects())[i] == nullptr) {
					(*getDinamicObjects())[i] = obj;
					return i;
				}
			}
		}

		void clearObjectToListMap();

	}
}
#endif // GAMEOBJECT_H
