#ifndef ENEMY_H
#define ENEMY_H

#ifdef __linux__
#include "GameObject/gameobject.h"
#else
#include "D:/WorkSpace/mygm/GameObject/gameobject.h"
//#include "D:/WorkSpace/mygm/GameObject/TypeObject/typeobject.h"
#endif // Qt

namespace Game {
    namespace GameObjects {
            class NPC : public Game::GameObjects::DinamicObject
            {
            public:
                NPC(Game::GameObjects::TypeObjects::TypeObject *r);

                bool update(minibson::document properti) override;

                minibson::document update() override;

                void draw(sf::RenderWindow *w) override;

                bool checkContactObject(long int i) override;

                bool animation();

                bool move();

                void newPointMove(sf::Vector2f p);

                void setWidget(Game::Widgets::Widget *w);

            private:
                Game::GameObjects::TypeObjects::TypeObject *type;
                bool flagClick = false;
                sf::View camera;
                Game::Widgets::Widget *widget = nullptr;
                sf::VertexArray pointsMove;
                //sf::IntRect rectMove;
                sf::RenderWindow *win;
                sf::Clock clokFindTime;
            };
    }
}

#endif // ENEMY_H
