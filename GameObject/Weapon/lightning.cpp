#ifdef __linux__
#include "GameObject/Weapon/lightning.h"
#else
#include "lightning.h"
#endif // Qt





Game::GameObjects::Weapons::Lightning::Lightning(sf::Vector2f p, std::string n, float r)
    : Weapon (p,n)
{
    if (!textureLigth.loadFromFile(static_cast<std::string>(DATA_PATH) + "sprite/share100x100.png"))
        std::cout << "Error" ;
    spriteLigth.setTexture(textureLigth);
    spriteLigth.scale(0.2,0.2);
    spriteLigth.setTextureRect(sf::IntRect(0,0,100,100));
    spriteLigth.setColor(sf::Color(0, 208, 255));
    spriteLigth.setOrigin(50,50);
    spriteLigth.setPosition(p);
    range = r;
    lines.setPrimitiveType(sf::LinesStrip);
    lines1.setPrimitiveType(sf::LinesStrip);
    lines2.setPrimitiveType(sf::LinesStrip);
}

void Game::GameObjects::Weapons::Lightning::draw(sf::RenderWindow *w)
{
    if(anim == 'A'){
        if(anim_count == -3){anim ='I';lines.clear();lines1.clear();lines2.clear();}
        else{
            if(lenLight<0){
                lines.append(sf::Vertex(posAttack,sf::Color(0, 208, 255)));
                lines1.append(sf::Vertex(posAttack,sf::Color(0, 208, 255)));
                lines2.append(sf::Vertex(posAttack,sf::Color(255, 208, 255)));
                spriteLigth.setPosition(posAttack);
                w->draw(spriteLigth);
                anim_count --;
            }
            else {
                for (int i=0;lenLight>0||i<8;i++) {
                    int l = (rand()% 20+5);
                    int d = 5;
                    lenLight -= l;
                    posNew.x += ((posAttack.x-posNew.x)/l)+((rand()% d+1)*(rand()% 2+1 == 1? 1 : -1));
                    posNew.y += ((posAttack.y-posNew.y)/l)+((rand()% d+1)*(rand()% 2+1 == 1? 1 : -1));
                    lines.append(sf::Vertex(posNew,sf::Color(0, 208, 255)));
                    posNew1.x += ((posAttack.x-posNew1.x)/l)+((rand()% d+1)*(rand()% 2+1 == 1? 1 : -1));
                    posNew1.y += ((posAttack.y-posNew1.y)/l)+((rand()% d+1)*(rand()% 2+1 == 1? 1 : -1));
                    lines1.append(sf::Vertex(posNew1,sf::Color(0, 208, 255)));
                    posNew2.x += ((posAttack.x-posNew2.x)/l)+((rand()% d+1)*(rand()% 2+1 == 1? 1 : -1));
                    posNew2.y += ((posAttack.y-posNew.y)/l)+((rand()% d+1)*(rand()% 2+1 == 1? 1 : -1));
                    lines2.append(sf::Vertex(posNew2,sf::Color(0, 208, 255)));
                }
            }
        }
        w->draw(lines);
        w->draw(lines1);
        w->draw(lines2);
        spriteLigth.setPosition(position);
        w->draw(spriteLigth);
    }
}

bool Game::GameObjects::Weapons::Lightning::move(sf::Vector2f s)
{
    return 0;
}

bool Game::GameObjects::Weapons::Lightning::attack(sf::Vector2f p)
{
    if(anim == 'I')
    {
        anim = 'A';
        posNew = position;
        posNew1 = position;
        posNew2 = position;
        clockDelayAttack.restart();
        float a = position.x - p.x;
        float b = position.y - p.y;
        lenLight = powf(powf(a,2)+powf(b,2),0.5);
        if(lenLight >range){
            a = a*(range/lenLight);
            b = b*(range/lenLight);
            posAttack = sf::Vector2f(position.x-a,position.y-b);
            lenLight = range;
        }
        else {
            posAttack = p;
        }
        lenLight+=100;
        lines.append(sf::Vertex(posNew,sf::Color::Yellow));
        lines1.append(sf::Vertex(posNew,sf::Color::Yellow));
        lines2.append(sf::Vertex(posNew,sf::Color::Yellow));
        anim_count = 1;
    }
    return 0;
}

bool Game::GameObjects::Weapons::Lightning::attackAlt(sf::Vector2f p)
{

    return 0;
}

bool Game::GameObjects::Weapons::Lightning::checkContactObject(long int i)
{

    return 0;
}
