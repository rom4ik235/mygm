#ifdef __linux__
#include "GameObject/Weapon/weapon.h"
#else
#include "weapon.h"
#endif // Qt


Game::GameObjects::Weapons::Weapon::Weapon(sf::Vector2f p, std::string n)
{
    position = p;
    nameWeapon = n;
}

Game::GameObjects::Weapons::Weapon::~Weapon()
{

}

void Game::GameObjects::Weapons::Weapon::draw(sf::RenderWindow *_window)
{

}

bool Game::GameObjects::Weapons::Weapon::move(sf::Vector2f s)
{
    return 0;
}

bool Game::GameObjects::Weapons::Weapon::attack(sf::Vector2f p)
{
    return 0;
}

bool Game::GameObjects::Weapons::Weapon::attackAlt(sf::Vector2f p)
{
    return 0;
}

bool Game::GameObjects::Weapons::Weapon::checkContactObject(long int i)
{
    return 0;
}

void Game::GameObjects::Weapons::Weapon::setPos(sf::Vector2f p)
{
    position = p;
}

void Game::GameObjects::Weapons::Weapon::setAnim(char a)
{
    anim = a;
}

char Game::GameObjects::Weapons::Weapon::getAnim()
{
    return anim;
}

sf::Vector2f Game::GameObjects::Weapons::Weapon::getPos()
{
    return position;
}
