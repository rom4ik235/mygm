#ifndef LIGHTNING_H
#define LIGHTNING_H

#ifdef __linux__
//#include "GameObject/gameobject.h"
#include "GameObject/Weapon/weapon.h"
#else
//#include "D:/WorkSpace/mygm/GameObject/gameobject.h"
#include "weapon.h"
#endif // Qt

namespace Game {
    namespace GameObjects {
        namespace Weapons{
            class Lightning : public Weapon
            {
            public:
                Lightning(sf::Vector2f p, std::string n = "Lightning", float r = 200);
                void draw(sf::RenderWindow *w) override;
                bool move(sf::Vector2f s)override;
                bool attack(sf::Vector2f p)override;
                bool attackAlt(sf::Vector2f p)override;
                bool checkContactObject(long int i)override;

            public:
                float lenLight;
                int anim_count = 0;
                sf::Clock clockDelayAttack;
                sf::Vector2f posNew;
                sf::Vector2f posNew1;
                sf::Vector2f posNew2;
                sf::Vector2f posAttack;
                sf::VertexArray  lines;
                sf::VertexArray  lines1;
                sf::VertexArray  lines2;
                sf::Texture textureLigth;
                sf::Sprite spriteLigth;
            };
        }
    }
}
#endif // LIGHTNING_H
