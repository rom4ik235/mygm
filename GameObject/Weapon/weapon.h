#ifndef WEAPON_H
#define WEAPON_H



#ifdef __linux__
#include "variable_storage.h"
#else
#include "D:/WorkSpace/mygm/variable_storage.h"
#endif // Qt

namespace Game {
    namespace GameObjects {
        namespace Weapons{
            class Weapon
            {
            public:
                Weapon(sf::Vector2f p, std::string n);
                virtual ~Weapon();
                virtual void draw(sf::RenderWindow *_window);
                virtual bool move(sf::Vector2f s);
                virtual bool attack(sf::Vector2f p);
                virtual bool attackAlt(sf::Vector2f p);
                virtual bool checkContactObject(long int i);

                void setPos(sf::Vector2f p);
                void setAnim(char a);

                sf::Vector2f getPos();
                char getAnim();

                std::string nameWeapon;
                float range;
            protected:
                sf::Vector2f position;
                char anim = 'I';
            };
    //        class Axe : public Weapon
    //        {
    //        public:
    //            Axe(sf::Vector2f _pos){
    //                if (!textureAxe.loadFromFile("/home/roma/Documents/workspace/sprite/Axe.png"))
    //                    std::cout << "Error" ;
    //                spriteAxe.setTexture(textureAxe);
    //                spriteAxe.setTextureRect(sf::IntRect(0,0,12,24));
    //                spriteAxe.setOrigin(6,12);
    //                spriteAxe.setPosition(_pos);

    //                location.setSize(sf::Vector2f(12,24));
    //                location.setFillColor(sf::Color(125,125,125,20));
    //                location.setPosition(_pos);
    //                location.setOrigin(6,12);

    //                anim = 'I';
    //            }
    //        private:
    //            sf::Sprite spriteAxe;
    //            sf::Texture textureAxe;
    //        };
        }
    }
}
#endif // WEAPON_H
