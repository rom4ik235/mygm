#include "Scene.h"

void clientConnect(std::string login, Game::Widgets::TextEdit* textAddress, Game::Widgets::TextEdit* textPort)
{
    std::cout << "clientConnect click " << std::endl;
    if(Game::getClientTCP() == nullptr)
        new Game::ClientTCP(login/*,textAddress->getText(),static_cast<unsigned short>(std::atoi(textPort->getText().c_str()))*/);
    Game::getClientTCP()->connect();
}


void sceneClient(std::string login)
{
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    Game::getInterface()->clearListWidget();
    m.unlock();

    sf::Vector2i grid = {20,20};
    sf::Vector2i centr = {(WIDHT/20)/2,(HEIGHT/20)/2};


    Game::Widgets::TextEdit* textAddress = new Game::Widgets::TextEdit(sf::Vector2f(),
                                                                     sf::Vector2f(),
                                                                     "192.168.10.35",
                                                                     sf::Color::Black,
                                                                     20);
    Game::Widgets::TextEdit* textPort = new Game::Widgets::TextEdit(sf::Vector2f(),
                                                                     sf::Vector2f(),
                                                                     "55001",
                                                                     sf::Color::Black,
                                                                     20);


    INIT_BUTTON(buttomBack) CREATE_BUTTON B.sizeText = 40; B.text = "Back"; END_CREATE;
    buttomBack->onClick = new sf::Thread([](){Game::Scene::sceneLogin();});

    INIT_BUTTON(buttomConnect) CREATE_BUTTON B.sizeText = 40; B.text = "Connect"; END_CREATE;
    buttomConnect->onClick = new sf::Thread(std::bind(&clientConnect,login,textAddress,textPort));

    Game::getInterface()->arrangeWidgetOnGrid(textAddress,grid,{centr.x-6,centr.y-8},{8,2});
    Game::getInterface()->arrangeWidgetOnGrid(textPort,grid,{centr.x+2,centr.y-8},{4,2});
    Game::getInterface()->arrangeWidgetOnGrid(buttomConnect,grid,{centr.x-5,centr.y-5},{10,4});
    Game::getInterface()->arrangeWidgetOnGrid(buttomBack,grid,{centr.x-5,centr.y},{10,4});
}

void sceneServer(std::string login)
{
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    Game::getInterface()->clearListWidget();
    m.unlock();

    if(Game::getServerTCP()==nullptr)
        new Game::ServerTCP();
    if(Game::getClientTCP()==nullptr)
        new Game::ClientTCP(login,"localhost");
    Game::getClientTCP()->connectServer = [](){
        minibson::document posPlayer;
        posPlayer.set("x",static_cast<double>(WIDHT/2));
        posPlayer.set("y",static_cast<double>(HEIGHT/2));
        minibson::document docplayer;
        docplayer.set("pos",posPlayer);
        Game::GameObjects::DinamicObject * testPlayer = new Game::GameObjects::NPC(new Game::GameObjects::TypeObjects::BallLightning(docplayer));
        Game::GameManager::geThis()->player.attachObject(Game::GameObjects::addObjectToListMap(testPlayer));
    };
    Game::getServerTCP()->waitNewSocket();
    Game::getClientTCP()->connect();
    sf::sleep(sf::seconds(1));

    minibson::document docMess;
    docMess.set("cmd","pasholNayy");
    docMess.set("Nomer",12356);
    docMess.set("float",0.256);
    docMess.set("bool",true);
    size_t size = docMess.get_serialized_size();
    char* buffer = new char[size];
    docMess.serialize(buffer, size);

    Game::getClientTCP()->send("bson",std::string(buffer,size));
    delete [] buffer;

    INIT_BUTTON(buttomBack) CREATE_BUTTON B.sizeText = 40; B.text = "Back"; END_CREATE;
    buttomBack->onClick = new sf::Thread([](){Game::Scene::sceneLogin();});
    Game::getInterface()->arrangeWidgetOnGrid(buttomBack,{20,20},{2,2},{10,4});

    INIT_BUTTON(buttomNewClient) CREATE_BUTTON B.sizeText = 40; B.text = "WaitNewClient"; END_CREATE;
    buttomNewClient->onClick = new sf::Thread([](){
        if(Game::getServerTCP()!=nullptr)
            Game::getServerTCP()->waitNewSocket();
    });
    Game::getInterface()->arrangeWidgetOnGrid(buttomNewClient,{20,20},{10,10},{10,4});

}

void choiceClientOrServer(Game::Widgets::TextEdit* editText)
{
    std::string login = editText->getText();
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    Game::getInterface()->clearListWidget();
    m.unlock();


    sf::Vector2i centr = {(WIDHT/20)/2,(HEIGHT/20)/2};


    INIT_BUTTON(buttomClient) CREATE_BUTTON B.sizeText = 40; B.text = "Client"; END_CREATE;
    buttomClient->onClick = new sf::Thread(sceneClient,login);
    Game::getInterface()->arrangeWidgetOnGrid(buttomClient,{20,20},{centr.x-5,centr.y-10},{10,4});

    INIT_BUTTON(buttomServer) CREATE_BUTTON B.sizeText = 40; B.text = "Server"; END_CREATE;
    buttomServer->onClick = new sf::Thread(sceneServer,login);
    Game::getInterface()->arrangeWidgetOnGrid(buttomServer,{20,20},{centr.x-5,centr.y-5},{10,4});

    INIT_BUTTON(buttomBack) CREATE_BUTTON B.sizeText = 40; B.text = "Back"; END_CREATE;
    buttomBack->onClick = new sf::Thread([](){Game::Scene::sceneLogin();});
    Game::getInterface()->arrangeWidgetOnGrid(buttomBack,{20,20},{centr.x-5,centr.y},{10,4});
}


void Game::Scene::sceneLogin()
{
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    Game::getInterface()->clearListWidget();
    m.unlock();



    Game::Widgets::TextEdit* textedit = new Game::Widgets::TextEdit(sf::Vector2f(),
                                                                     sf::Vector2f(),
                                                                     "Player1",
                                                                     sf::Color::Black,
                                                                     20);

    INIT_BUTTON(buttomAccept) CREATE_BUTTON B.sizeText = 40; B.text = "Accept"; END_CREATE;
    buttomAccept->onClick = new sf::Thread(choiceClientOrServer,textedit);
    INIT_BUTTON(buttomTest) CREATE_BUTTON B.sizeText = 40; B.text = "Testing"; END_CREATE;
    buttomTest->onClick = new sf::Thread([](){Game::Scene::scenePause();});

    sf::Vector2i centr = {(WIDHT/20)/2,(HEIGHT/20)/2};
    Game::getInterface()->arrangeWidgetOnGrid(textedit,{20,20},{centr.x-4,centr.y-10},{8,2});
    Game::getInterface()->arrangeWidgetOnGrid(buttomAccept,{20,20},{centr.x-5,centr.y-7},{10,4});
    Game::getInterface()->arrangeWidgetOnGrid(buttomTest,{20,20},{centr.x-5,centr.y-7+5},{10,4});
}
