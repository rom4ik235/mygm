#include "Scene.h"

void createClient(Game::Widgets::TextEdit * editLogin)
{
    if(Game::getClientTCP()==nullptr)
        new Game::ClientTCP(editLogin->getText());
    Game::getClientTCP()->connect();
}

void messageServer(Game::Widgets::ComboBox  * editLogin,Game::Widgets::TextEdit * mess)
{
    if(Game::getServerTCP()!=nullptr)
        Game::getServerTCP()->send(editLogin->getText(),mess->getText());
}

std::vector<std::string> comboBoxLogins()
{
    if(Game::getServerTCP()!=nullptr)
        return  Game::getServerTCP()->getActiveConnect();
    return std::vector<std::string>();
}

void messageClient(Game::Widgets::TextEdit * editLogin)
{
    if(Game::getClientTCP()!=nullptr)
        Game::getClientTCP()->send(editLogin->getText(),editLogin->getText());
}

void Game::Scene::sceneTestServerOrClient()
{
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    Game::getInterface()->clearListWidget();
    m.unlock();

    INIT_BUTTON(buttonBack) CREATE_BUTTON B.sizeText = 40; B.text = "Back"; END_CREATE;
    buttonBack->onClick = new sf::Thread([]() {Game::Scene::sceneLogin();});


    INIT_BUTTON(buttonServerStart) CREATE_BUTTON B.sizeText = 40; B.text = "ServerStart"; END_CREATE;

    buttonServerStart->onClick = new sf::Thread([]() {
        if(Game::getServerTCP()==nullptr){
            new Game::ServerTCP();
        }
        Game::getServerTCP()->waitNewSocket();
    });

    INIT_BUTTON(buttonClientStart) CREATE_BUTTON B.sizeText = 40; B.text = "ClientStart"; END_CREATE;

    Game::Widgets::TextEdit * editLogin = new Game::Widgets::TextEdit(sf::Vector2f(),sf::Vector2f(),"rom4ik234",sf::Color::Blue,30);

    buttonClientStart->onClick = new sf::Thread(std::bind( &createClient, editLogin));

    Game::Widgets::TextEdit * editMesServer = new Game::Widgets::TextEdit(sf::Vector2f(),sf::Vector2f(),"Hi",sf::Color::Blue,30);
    Game::Widgets::TextEdit * editMesClient = new Game::Widgets::TextEdit(sf::Vector2f(),sf::Vector2f(),"Hi",sf::Color::Blue,30);

    Game::Widgets::ComboBox * ComboBoxLoginList = new Game::Widgets::ComboBox(sf::Vector2f(),0,0,25);
    ComboBoxLoginList->setList = comboBoxLogins;
//    ComboBoxLoginList->addRowString("Global");

    INIT_BUTTON(buttonServerSend) CREATE_BUTTON B.sizeText = 40; B.text = "ServerSend"; END_CREATE;
    INIT_BUTTON(buttonClientSend) CREATE_BUTTON B.sizeText = 40; B.text = "ClientSend"; END_CREATE;

    buttonServerSend->onClick = new sf::Thread(std::bind(&messageServer,ComboBoxLoginList,editMesServer));
    buttonClientSend->onClick = new sf::Thread(std::bind(&messageClient,editMesClient));

    Game::getInterface()->arrangeWidgetOnGrid(buttonBack,           { 20,20 }, { 4,4 },   {10,5});
    Game::getInterface()->arrangeWidgetOnGrid(buttonServerStart,    { 20,20 }, { 4,10 },  {10,5});
    Game::getInterface()->arrangeWidgetOnGrid(buttonServerSend,     { 20,20 }, { 26,10 }, {10,5});
    Game::getInterface()->arrangeWidgetOnGrid(editMesServer,        { 20,20 }, { 37,10 }, {10,5});
    Game::getInterface()->arrangeWidgetOnGrid(ComboBoxLoginList,    { 20,20 }, { 37,4 },  {10,5});

    Game::getInterface()->arrangeWidgetOnGrid(editLogin,            { 20,20 }, { 15,16 }, {10,5});
    Game::getInterface()->arrangeWidgetOnGrid(buttonClientStart,    { 20,20 }, { 4,16 },  {10,5});
    Game::getInterface()->arrangeWidgetOnGrid(buttonClientSend,     { 20,20 }, { 26,16 }, {10,5});
    Game::getInterface()->arrangeWidgetOnGrid(editMesClient,        { 20,20 }, { 37,16 }, {10,5});



}
