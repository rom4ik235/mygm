#include "Scene.h"

void Game::Scene::sceneTestGrid()
{
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    Game::getInterface()->clearListWidget();
    m.unlock();

    INIT_BUTTON(buttonBack) CREATE_BUTTON B.sizeText = 40; B.text = "Back"; END_CREATE;
    buttonBack->onClick = new sf::Thread([]() {Game::Scene::sceneLogin();});

    Game::getInterface()->arrangeWidgetOnGrid(buttonBack, { 20,20 }, { 4,4 }, {10,5});




}
