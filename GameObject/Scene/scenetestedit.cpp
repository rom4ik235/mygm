#include "Scene.h"

void Game::Scene::sceneTestTextEdit()
{
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    Game::getInterface()->clearListWidget();
    m.unlock();

    INIT_BUTTON (buttonBack)
            CREATE_BUTTON
            B.text = "Back";B.colorButtom = sf::Color::Yellow; B.position ={20,20}; B.sizeText = 40;
    END_CREATE;


    auto back = []() {
        Game::Scene::sceneLogin();
    };
    buttonBack->onClick = new sf::Thread(back);
}


