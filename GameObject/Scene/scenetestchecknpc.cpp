#include "Scene.h"

void Game::Scene::sceneTestCheckNPC()
{
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    Game::getInterface()->clearListWidget();
    m.unlock();
}
