#include "Scene.h"

void Game::Scene::sceneTestLayout()
{
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    Game::getInterface()->clearListWidget();
    m.unlock();

    Game::Widgets::Button* buttonBack = new Game::Widgets::Button("Back",
                                                                  sf::Vector2f(20, 20),
                                                                  sf::Vector2f(),
                                                                  sf::Color::White,
                                                                  sf::Color::Black,
                                                                  40,3);
    auto back = []() {
        Game::Scene::sceneLogin();
    };
    buttonBack->onClick = new sf::Thread(back);
    
    Game::Widgets::Button* buttonButton1 = new Game::Widgets::Button("Button1",
                                                                     sf::Vector2f(20, 20),
                                                                     sf::Vector2f(),
                                                                     sf::Color::White,
                                                                     sf::Color::Black,
                                                                     40,3);
    auto button1 = []() {
        printf("button1");
    };
    buttonButton1->onClick = new sf::Thread(button1);


    Game::Widgets::Button* buttonButton2 = new Game::Widgets::Button("Button2",
                                                                     sf::Vector2f(20, 20),
                                                                     sf::Vector2f(),
                                                                     sf::Color::White,
                                                                     sf::Color::Black,
                                                                     40,3);
    auto button2 = []() {
        printf("button2");
    };
    buttonButton2->onClick = new sf::Thread(button2);


    Game::Widgets::Button* buttonButton3 = new Game::Widgets::Button("Button3",
                                                                     sf::Vector2f(20, 20),
                                                                     sf::Vector2f(),
                                                                     sf::Color::White,
                                                                     sf::Color::Black,
                                                                     40,3);
    auto button3 = []() {
        printf("button3");
    };
    buttonButton3->onClick = new sf::Thread(button3);
    //Game::getInterface()->addWidgetToListMap(buttonBack);
    //Game::getInterface()->addWidgetToListMap(buttonButton1);
    //Game::getInterface()->addWidgetToListMap(buttonButton2);
    //Game::getInterface()->addWidgetToListMap(buttonButton3);
    Game::Widgets::TextEdit* textEdit1 = new Game::Widgets::TextEdit(sf::Vector2f(200, 200), sf::Vector2f(200, 100), "Enter text...", sf::Color::Black, 20);
    Game::Widgets::Layout* lauout = new Game::Widgets::Layout(sf::Vector2f(100,200),311,233,2,2);

    Game::Widgets::ComboBox* comboBox = new Game::Widgets::ComboBox(sf::Vector2f(450, 200), 200, 50, 25);
    comboBox->addRowString("test1");
    comboBox->addRowString("testTesta1");
    comboBox->addRowString("testDlaTesta1");
    comboBox->addRowString("testDlaTesta1Podlenee");

    lauout->addWidget(buttonBack);
    lauout->addWidget(comboBox);
    lauout->addWidget(buttonButton1);
    lauout->addWidget(textEdit1);
    lauout->addWidget(buttonButton2);
    lauout->addWidget(buttonButton3);

    Game::getInterface()->addWidgetToListMap(lauout);

    Game::Widgets::CheckBox *check = new Game::Widgets::CheckBox("strannaya hueta", sf::Vector2f (450,500),sf::Vector2f (150,30),sf::Color::White,sf::Color::Black,15);
    Game::getInterface()->addWidgetToListMap(check);

}
