#include "Scene.h"

void Game::Scene::sceneTestMove()
{
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    m.unlock();
}
