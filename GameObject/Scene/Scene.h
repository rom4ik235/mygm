#ifndef SCENE_PAUSE_HPP
#define SCENE_PAUSE_HPP


#ifdef __linux__
#include "GameManager.h"
#else
#include  "D:/WorkSpace/mygm/GameManager.h"
#endif // Qt

#define PARAM_BUTTON [](){ Game::Widgets::ButtonParam B;
#define INIT_BUTTON(b) Game::Widgets::Button* b =
#define CREATE_BUTTON new Game::Widgets::Button(PARAM_BUTTON
#define END_CREATE return B;}())


namespace  Game{
    namespace  Scene{
        void scenePause();
        void sceneTestMove();
        void sceneTestCheckNPC();
        void sceneTestLayout();
        void sceneTestTextEdit();
        void sceneTestGrid();
        void sceneGame();
        void sceneTestServerOrClient();
        void sceneLogin();
    }
}


#endif // SCENE_PAUSE_HPP
