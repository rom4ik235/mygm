#include "Scene.h"

void Game::Scene::scenePause()
{
    sf::Mutex m;
    m.lock();
    Game::getInterface()->clearListWidget();
    Game::GameObjects::clearObjectToListMap();
    Game::GameObjects::clearStaticObject();
    m.unlock();


        Game::Widgets::Button* buttonBack = new Game::Widgets::Button("Back",
                                                                      sf::Vector2f(20, 20),
                                                                      sf::Vector2f(),
                                                                      sf::Color::White,
                                                                      sf::Color::Black,
                                                                      40);
        auto back = []() {
            Game::Scene::sceneLogin();
        };
        buttonBack->onClick = new sf::Thread(back);
        Game::getInterface()->addWidgetToListMap(buttonBack);
}
