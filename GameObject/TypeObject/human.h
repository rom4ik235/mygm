#ifndef HUMAN_H
#define HUMAN_H

#ifdef __linux__
#include "GameObject/TypeObject/typeobject.h"
#else
#include "typeobject.h"
#endif // Qt

namespace Game {
    namespace GameObjects {
        namespace TypeObjects {
            class Human : public Game::GameObjects::TypeObjects::TypeObject
            {
            public:
                Human(minibson::document property/*, const char* n = "Human"*/);

                void draw(sf::RenderWindow *_window) override;

                bool attackLeft(sf::Vector2f p)override;
                bool attackRight(sf::Vector2f p)override;

                bool move(sf::Vector2f s)override;

                bool animation();

            private:
//                sf::Texture textureAxe;
//                sf::Texture textureShield;
                float speed;
                sf::Vector2f speedVector;
                float delta;
                int angle_rotation;
                int sign_rotation;
                sf::Vector2f posMove;
                sf::Texture textureHuman;
//                sf::Sprite spriteAxe;
//                sf::Sprite spriteShield;
                sf::Sprite spriteHuman;
                sf::Clock clokAttack;
                sf::Clock clokBlock;
                int animationCount = 0;
            };
        }
    }
}

#endif // HUMAN_H
