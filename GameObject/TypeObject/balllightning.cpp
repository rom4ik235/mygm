#ifdef __linux__
#include "GameObject/TypeObject/balllightning.h"
#else
#include "balllightning.h"
#endif // Qt

Game::GameObjects::TypeObjects::BallLightning::BallLightning(minibson::document property/*, const char* n*/):
    TypeObject(property/*,n*/)
{
    if (!textureModel.loadFromFile(static_cast<std::string>(DATA_PATH) + "sprite/share100x100.png"))
        std::cout << "Error" ;

    sf::Vector2f p(static_cast<float>(property.get("pos",minibson::document()).get("x",0.0)),
                   static_cast<float>(property.get("pos",minibson::document()).get("y",0.0)));
    spriteBody.setTexture(textureModel);
    spriteBody.scale(0.5,0.5);
    spriteBody.setTextureRect(sf::IntRect(0,0,100,100));
    spriteBody.setColor(sf::Color(1,1,255));
    spriteBody.setOrigin(50,50);
    spriteBody.setPosition(p);

    spriteShadow.setTexture(textureModel);
    spriteShadow.setTextureRect(sf::IntRect(100,0,100,100));
    spriteShadow.setColor(sf::Color(1,1,255));
    spriteShadow.setOrigin(50,50);
    spriteShadow.setPosition(p);
    clokAnim.restart();

    Weapon1 = new Game::GameObjects::Weapons::Lightning(p,"",100);
}

void Game::GameObjects::TypeObjects::BallLightning::draw(sf::RenderWindow *w)
{
    if(clokAnim.getElapsedTime().asMilliseconds()>100){
        animationCount= rand() % 9 + 1;
        spriteShadow.setTextureRect(sf::IntRect(animationCount*100,0,100,100));
        spriteShadow.setRotation((rand() % 90 + (-90)));
        clokAnim.restart();
    }
    if(anim=='A')
        anim = Weapon1->getAnim();
    Weapon1->draw(w);
    w->draw(spriteShadow);
    w->draw(spriteBody);
}

bool Game::GameObjects::TypeObjects::BallLightning::move(sf::Vector2f s)
{
    spriteBody.move(s);
    spriteShadow.move(s);
    return 0;
}

void Game::GameObjects::TypeObjects::BallLightning::setPos(sf::Vector2f p)
{
    spriteBody.setPosition(p);
    spriteShadow.setPosition(p);
}

bool Game::GameObjects::TypeObjects::BallLightning::attackLeft(sf::Vector2f p)
{
    Weapon1->setPos(sf::Vector2f(spriteBody.getPosition().x,spriteBody.getPosition().y));
//    std::cout << spriteBody.getPosition().x << "x  sBody  y" << spriteBody.getPosition().y << std::endl;
    Weapon1->attack(p);
    anim = Weapon1->getAnim();
    return 0;
}

float Game::GameObjects::TypeObjects::BallLightning::getRadiusAttack()
{
    return Weapon1->range;
}
