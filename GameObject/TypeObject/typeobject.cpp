

#ifdef __linux__
#include "GameObject/TypeObject/typeobject.h"
#include "GameObject/StaticObject/staticobject.h"
#else
#include "typeobject.h"
#endif // Qt

Game::GameObjects::TypeObjects::TypeObject::TypeObject(minibson::document property/*, const char *n*/) /*: nameRace(n)*/
{
    position = {static_cast<float>(property.get("pos",minibson::document()).get("x",0.0)),
                static_cast<float>(property.get("pos",minibson::document()).get("y",0.0))};
    areaHitRadius = 25;
    gain = 2;
}

Game::GameObjects::TypeObjects::TypeObject::~TypeObject()
{

}


void Game::GameObjects::TypeObjects::TypeObject::draw(sf::RenderWindow *w)
{

}


bool Game::GameObjects::TypeObjects::TypeObject::move(sf::Vector2f s)
{
    return 0;
}

bool Game::GameObjects::TypeObjects::TypeObject::attackLeft(sf::Vector2f p)
{
    return 0;
}

bool Game::GameObjects::TypeObjects::TypeObject::attackRight(sf::Vector2f p)
{
    return 0;
}

void Game::GameObjects::TypeObjects::TypeObject::setPos(sf::Vector2f p)
{
    position = p;
}

bool Game::GameObjects::TypeObjects::TypeObject::checkContactObject(long i)
{
    return 0;
}

float Game::GameObjects::TypeObjects::TypeObject::getRadiusAttack()
{
    return 0;
}

sf::Vector2f Game::GameObjects::TypeObjects::TypeObject::getPos()
{
    return position;
}

char Game::GameObjects::TypeObjects::TypeObject::getAnim()
{
    return anim;
}

void Game::GameObjects::TypeObjects::TypeObject::setAnim(char a)
{
    anim = a;
}

