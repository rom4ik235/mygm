#ifndef TYPEOBJECT_H
#define TYPEOBJECT_H

#ifdef __linux__
#include "GameObject/Weapon/weapon.h"
#include "GameObject/Weapon/lightning.h"
#else
#include "D:/WorkSpace/mygm/GameObject/Weapon/weapon.h"
#include "D:/WorkSpace/mygm/GameObject/Weapon/lightning.h"
#endif // Qt

namespace Game {
    namespace GameObjects {
        namespace TypeObjects {
            class  TypeObject
            {
            public:
                TypeObject(minibson::document property/*, const char* n*/);
                virtual ~TypeObject();
                virtual void draw(sf::RenderWindow *w);
                virtual bool move(sf::Vector2f s);
                virtual bool attackLeft(sf::Vector2f p);
                virtual bool attackRight(sf::Vector2f p);
                virtual void setPos(sf::Vector2f p);
                virtual bool checkContactObject(long int i);
                virtual float getRadiusAttack();
                void setAnim(char a);
                char getAnim();
                sf::Vector2f getPos();
                const char* nameRace ="";
                float gain;
                float areaHitRadius;
                int health = 100;
                Game::GameObjects::Weapons::Weapon *Weapon1 = nullptr;
                Game::GameObjects::Weapons::Weapon *Weapon2 = nullptr;
            protected:
                sf::Vector2f position;
                char anim = 'I';
            };
        }
    }
}

#endif // TYPEOBJECT_H
