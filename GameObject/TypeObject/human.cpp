#ifdef __linux__
#include "GameObject/TypeObject/human.h"
#else
#include "human.h"
#endif // Qt

Game::GameObjects::TypeObjects::Human::Human(minibson::document property/*, const char *n*/):
    TypeObject(property/*,n*/)
{
//    if (!textureAxe.loadFromFile("/home/roma/Documents/workspace/sprite/Axe.png"))
//        std::cout << "Error" ;
//    if (!textureShield.loadFromFile("/home/roma/Documents/workspace/sprite/shield.png"))
//        std::cout << "Error" ;
    if (!textureHuman.loadFromFile(static_cast<std::string>(DATA_PATH) + "sprite/Human_pixel.png"))
        std::cout << "Error" ;


//    spriteShield.setTexture(textureShield);
//    spriteShield.setTextureRect(sf::IntRect(0,0,24,4));
//    spriteShield.scale(2,2);
//    spriteShield.setOrigin(12,20);
//    spriteShield.setRotation(180);
//    spriteShield.setPosition(p);

//    spriteAxe.setTexture(textureAxe);
//    spriteAxe.setTextureRect(sf::IntRect(0,0,12,24));
//    spriteAxe.setOrigin(6,34);
//    spriteAxe.scale(2,2);
//    spriteAxe.setRotation(-90);
//    spriteAxe.setPosition(p);

    spriteHuman.setTexture(textureHuman);
    spriteHuman.setTextureRect(sf::IntRect(0,0,40,40));
    spriteHuman.setOrigin(20,20);
    spriteHuman.setRotation(90);
//    spriteHuman.setPosition(p);
    spriteHuman.scale(2,2);
    angle_rotation = 0;
    speed = 1;
    delta = 0;
}

void Game::GameObjects::TypeObjects::Human::draw(sf::RenderWindow *_window)
{
    spriteHuman.setRotation(angle_rotation);
    _window->draw(spriteHuman);
}

bool Game::GameObjects::TypeObjects::Human::attackLeft(sf::Vector2f p)
{
    if(clokAttack.getElapsedTime().asMilliseconds()>200){anim='A';}
    return 0;
}

bool Game::GameObjects::TypeObjects::Human::attackRight(sf::Vector2f p)
{
    if(clokAttack.getElapsedTime().asMilliseconds()>200){anim='B';}
    return 0;
}

bool Game::GameObjects::TypeObjects::Human::move(sf::Vector2f s)
{
    if(s!=sf::Vector2f{0,0}&&posMove!=s){
        angle_rotation = ((atan2f(s.y,s.x))*180/Pi)+90;
        posMove = s;
    }
    spriteHuman.move(s);
    return 0;
}


bool Game::GameObjects::TypeObjects::Human::animation(){
    if (anim=='A'){
        clokAttack.restart();
        if(animationCount==20){
//            spriteAxe.setRotation(-90);
            animationCount = 0;
            anim='I';
            clokAttack.restart();
            return 0;
        }
//        spriteAxe.setRotation(spriteAxe.getRotation()+10);
        animationCount++;
    }
    else if (anim=='B'){
        if(animationCount==20){
//            spriteShield.setRotation(spriteShield.getRotation()+90);
            animationCount = 0;
            anim='I';
            clokAttack.restart();
            return 0;
        }
        if(animationCount==0)
//            spriteShield.setRotation(spriteShield.getRotation()-90);
        animationCount++;
    }
    return 0;
}
