#ifndef BALLLIGHTNING_H
#define BALLLIGHTNING_H

#ifdef __linux__
#include "GameObject/TypeObject/typeobject.h"
#else
#include "typeobject.h"
#endif // Qt

namespace Game {
    namespace GameObjects {
        namespace TypeObjects {
            class BallLightning : public Game::GameObjects::TypeObjects::TypeObject
            {
            public:
                BallLightning(minibson::document property/*, const char* n = "BallLightning"*/);
                void draw(sf::RenderWindow *w) override;
                bool move(sf::Vector2f s) override;
                void setPos(sf::Vector2f p) override;
                bool attackLeft(sf::Vector2f p)override;
                float getRadiusAttack() override;
            private:
                sf::Texture textureModel;
                sf::Sprite spriteBody;
                sf::Sprite spriteShadow;
                sf::Clock clokAttack;
                sf::Clock clokAnim;
                int animationCount = 0;
            };
        }
    }
}
#endif // BALLLIGHTNING_H
