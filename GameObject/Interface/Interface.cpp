#include "Interface.h"
static Game::Interface *interface = nullptr;

Game::Interface::Interface(sf::RenderWindow* w)
{
    window = w;
    if(interface == nullptr)
        interface = this;
    cursor.setRadius(5);
    cursor.setFillColor(sf::Color(0,255,0,100));
    cursor.setOrigin(5,5);
}

void Game::Interface::update(sf::Event event)
{
    setPositionMouse(window->mapPixelToCoords(sf::Mouse::getPosition(*window)));
    for (auto it = list_widget.begin(); it != list_widget.end(); it++)
    {
        it->second->update(event);
    }
}

void Game::Interface::draw()
{
    setPositionMouse(window->mapPixelToCoords(sf::Mouse::getPosition(*window)));
    cursor.setPosition(getPositionMouse());
	for (auto it = list_widget.begin(); it != list_widget.end(); it++)
	{
        it->second->setPositionWidget(window->mapPixelToCoords(it->second->getPosition()));
		it->second->draw(window);
	}
    window->draw(cursor);
}

void Game::Interface::thredInterface()
{
	while (window->isOpen()) {
        sf::Event event;
        window->pollEvent(event);
		for (auto it = list_widget.begin(); it != list_widget.end(); it++)
		{
            it->second->update(event);
		}
        sf::sleep(sf::milliseconds(100));
    }
}

bool Game::Interface::removeWidget(long i)
{
    list_widget.erase(i);
    return 0;
}

bool Game::Interface::clearListWidget()
{
    list_widget.clear();
    return 0;
}

void Game::Interface::arrangeWidgetOnGrid(Game::Widgets::Widget* w, sf::Vector2i sizeGrid, sf::Vector2i pos, sf::Vector2i size)
{
    w->setSize(sf::Vector2f(sizeGrid.x * size.x, sizeGrid.y * size.y));
    w->setPosition(sf::Vector2i(pos.x * sizeGrid.x, sizeGrid.y * pos.y));
    addWidgetToListMap(w);
}

Game::Interface *Game::getInterface()
{
    return interface;
}
