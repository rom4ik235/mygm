#ifndef WIDGET_H
#define WIDGET_H

#ifdef __linux__
#include "variable_storage.h"
#else
#include "D:/WorkSpace/mygm/variable_storage.h"
#endif 

namespace Game {
	namespace Widgets {
		class Widget
		{
		public:
			virtual ~Widget();
		
            virtual void update(sf::Event event);
			
			virtual void draw(sf::RenderWindow *w);

            virtual void setPositionWidget(sf::Vector2f p);

			virtual void setSize(sf::Vector2f s);

            virtual void move(sf::Vector2f s);

            virtual void setOrigin(sf::Vector2f p);

            virtual void setPosition(sf::Vector2i p);

			sf::Vector2i getPosition();

		protected:
			sf::Vector2i position;
		};
	}
	void setPositionMouse(sf::Vector2f p);
	sf::Vector2f getPositionMouse();

}

#endif // WIDGET_H
