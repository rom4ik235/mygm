#ifndef WIDGET_LAYOUT_H
#define WIDGET_LAYOUT_H

#include "Widget.h"

namespace Game {
    namespace Widgets {
        class Layout : public Game::Widgets::Widget
        {
        public:
            Layout(sf::Vector2f p, float w, float h, size_t row = 0, size_t column = 0, sf::Color c = sf::Color::Black);

            ~Layout() override;

            void update(sf::Event event) override;

            void draw(sf::RenderWindow* w) override;

            void setPositionWidget(sf::Vector2f p) override;

            void addWidget(Game::Widgets::Widget * w);

            void setSize(sf::Vector2f s) override;

            void setOrigin(sf::Vector2f p) override;

            void setPosition(sf::Vector2i p) override;

        private:
            sf::RectangleShape rectLayout;
            std::vector<Game::Widgets::Widget*> widgets;
            size_t countColumn = 0;
            size_t countRow = 0;
            float widhtWidget = 0;
            float heightWidget = 0;
        };
    }
}

#endif // WIDGET_LAYOUT_H
