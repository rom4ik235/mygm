#include "Widget.h"

Game::Widgets::Widget::~Widget()
{
}

void Game::Widgets::Widget::update(sf::Event event)
{
}

void Game::Widgets::Widget::draw(sf::RenderWindow* w)
{
}

void Game::Widgets::Widget::setPositionWidget(sf::Vector2f p)
{
}

void Game::Widgets::Widget::move(sf::Vector2f p)
{

}

void Game::Widgets::Widget::setOrigin(sf::Vector2f p)
{

}

void Game::Widgets::Widget::setSize(sf::Vector2f s)
{
}

void Game::Widgets::Widget::setPosition(sf::Vector2i p)
{
	position = p;
}

sf::Vector2i Game::Widgets::Widget::getPosition()
{
	return position;
}

static sf::Vector2f positionMouse = { 0,0 };

void Game::setPositionMouse(sf::Vector2f p )
{
	positionMouse = p;
}

sf::Vector2f Game::getPositionMouse()
{
	return positionMouse;
}
