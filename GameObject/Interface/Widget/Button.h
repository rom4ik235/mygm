#ifndef WIDGET_BUTTON_H
#define WIDGET_BUTTON_H

#include "Widget.h"

namespace Game {
	namespace Widgets {

        struct ButtonParam
        {
            std::string text = "";
            sf::Vector2f position = {};
            sf::Vector2f size = {};
            sf::Color colorButtom = sf::Color::White;
            sf::Color colorText = sf::Color::Black;
            sf::Color colorBorder = sf::Color::Black;
            unsigned sizeText = 20;
            float border = 3;
            float indent = 5;
        };

		class Button : public Game::Widgets::Widget
		{
		public:

            Button(std::string t,sf::Vector2f p, sf::Vector2f s, sf::Color cb, sf::Color ct, unsigned int st, float b = 3 , float i = 5, sf::Color cbrd = sf::Color::Black);

            Button(ButtonParam bp);

            ~Button() override;

            void update(sf::Event event) override;

			void draw(sf::RenderWindow* w) override;

            void setPositionWidget(sf::Vector2f p) override;

            void setSize(sf::Vector2f s) override;

            void setOrigin(sf::Vector2f p) override;

            void move(sf::Vector2f s) override;

            sf::Thread *onClick = nullptr;

            void setText(std::string t, unsigned size = 0);

            std::string getText();

            void setColorBorder(sf::Color c);

            void setColorText(sf::Color c);

            void setColorBackGround(sf::Color c);

		private:
            sf::RectangleShape rectButton;
            sf::RectangleShape rectBorder;
            sf::Text textButton;
            sf::Color color;
            float indent = 5;
            float border = 5;

            sf::Font font;
            float failTextY;
            float failTextX;
            sf::Clock clokClick;
			bool flagPress;
            bool flagFocused;
		};
	}
}

#endif // WIDGET_BUTTON_H
