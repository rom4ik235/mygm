#include "CheckBox.h"

Game::Widgets::CheckBox::CheckBox(std::string t, sf::Vector2f p, sf::Vector2f s, sf::Color cb, sf::Color ct, unsigned int st, float b, float i, sf::Color cbrd)
{
    position = static_cast<sf::Vector2i> (p);
    border = b;
    indent = i;

    rectBorderText.setSize(s);
    rectBorderText.setFillColor(sf::Color::Black);
    rectBorderText.setPosition(p);

    rectText.setSize({(s.x-(border*2)),(s.y-(border*2))});
    rectText.setFillColor(cb);
    rectText.setOrigin(-border,-border);
    rectText.setPosition(p);

    rectBorder.setSize({(s.y-(border*2))*0.8f,(s.y-(border*2))*0.8f});
    rectBorder.setOrigin({-(s.x-(border*2))+rectBorder.getSize().x,
                          -(s.y/2)+rectBorder.getSize().y/2});
    rectBorder.setFillColor(cbrd);
    rectBorder.setPosition(p);

    rectBox.setSize({rectBorder.getSize().x*0.8f,rectBorder.getSize().y*0.8f});
    rectBox.setFillColor(sf::Color::White);
    rectBox.setOrigin((-rectBorder.getSize().x*0.1f)+rectBorder.getOrigin().x,(-rectBorder.getSize().y*0.1f)+rectBorder.getOrigin().y);
    rectBox.setPosition(p);

    rectCheck.setSize({rectBorder.getSize().x*0.6f,rectBorder.getSize().y*0.6f});
    rectCheck.setFillColor(sf::Color(80,80,20));
    rectCheck.setOrigin((-rectBorder.getSize().x*0.2f)+rectBorder.getOrigin().x,(-rectBorder.getSize().y*0.2f)+rectBorder.getOrigin().y);
    rectCheck.setPosition(p);

    if(!font.loadFromFile(static_cast<std::string>(DATA_PATH) + "Font/arial.ttf")){
       std::cout << "Error" ;
    }
    textCheck.setFont(font);
    textCheck.setCharacterSize(st);
    textCheck.setStyle(sf::Text::Style::Regular);
    textCheck.setFillColor(ct);
    textCheck.setString(t);
    sf::FloatRect rect = textCheck.getGlobalBounds();
    failTextX = textCheck.getGlobalBounds().left - textCheck.getPosition().x;
    failTextY = textCheck.getGlobalBounds().top - textCheck.getPosition().y;
    textCheck.setPosition(p);
    textCheck.setOrigin(-(border*2) + failTextX,
                        -(rectBorder.getSize().y/2)+(rect.height/2) +border- failTextY);
}

Game::Widgets::CheckBox::~CheckBox()
{

}

void Game::Widgets::CheckBox::update(sf::Event event)
{
    if(event.type == sf::Event::MouseButtonReleased){
        if (rectBorder.getGlobalBounds().contains(Game::getPositionMouse())){
            if(event.mouseButton.button == sf::Mouse::Left){
                flagPress = !flagPress;
            }
        }
    }
}

void Game::Widgets::CheckBox::draw(sf::RenderWindow *w)
{
    w->draw(rectBorderText);
    w->draw(rectText);
    w->draw(textCheck);
    w->draw(rectBorder);
    w->draw(rectBox);
    if (flagPress == true){
        w->draw(rectCheck);
    }


}

void Game::Widgets::CheckBox::setPositionWidget(sf::Vector2f p)
{

}

void Game::Widgets::CheckBox::setSize(sf::Vector2f s)
{

}

void Game::Widgets::CheckBox::setOrigin(sf::Vector2f p)
{

}

void Game::Widgets::CheckBox::move(sf::Vector2f s)
{

}

void Game::Widgets::CheckBox::setText(std::string t, unsigned size)
{


}

std::string Game::Widgets::CheckBox::getText()
{
    return "";
}

void Game::Widgets::CheckBox::setColorBorder(sf::Color c)
{

}

void Game::Widgets::CheckBox::setColorText(sf::Color c)
{

}

void Game::Widgets::CheckBox::setColorBackGround(sf::Color c)
{

}
