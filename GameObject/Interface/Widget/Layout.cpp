#include "Layout.h"

Game::Widgets::Layout::Layout(sf::Vector2f p,float w,float h, size_t row,size_t column, sf::Color c)
{
	rectLayout.setSize(sf::Vector2f(w, h));
	rectLayout.setFillColor(c);
    rectLayout.setPosition(p);
    position = static_cast<sf::Vector2i>(p);
    countColumn = column;
    countRow = row;
}

Game::Widgets::Layout::~Layout()
{
}

void Game::Widgets::Layout::update(sf::Event event)
{
    for (size_t i = 0; i < (countColumn*countRow); i++) {
        if (widgets.size() - 1 < i)
            break;
        widgets[i]->update(event);
    }
}

void Game::Widgets::Layout::draw(sf::RenderWindow* w)
{
    for (size_t i = 0; i < countColumn * countRow; i++) {
        if (widgets.size() - 1 < i)
            break;
        widgets[i]->draw(w);
    }
}

void Game::Widgets::Layout::setSize(sf::Vector2f s)
{
    rectLayout.setSize(s);
    size_t countWidget =  widgets.size();

    if (countRow != 0 && countColumn != 0 && countRow * countColumn >= countWidget)
    {
        widhtWidget = rectLayout.getSize().x / countColumn;
        heightWidget = rectLayout.getSize().y / countRow;
    }
    else if(countRow == 0 && countColumn == 0)
    {
        if (countWidget % 2 != 0)
            countWidget += 1;
        countColumn = countWidget / 2;
        countRow = countWidget - countColumn;
        widhtWidget = rectLayout.getSize().x / countRow;
        heightWidget = rectLayout.getSize().y / countColumn;
    }
    else if(countRow == 0 && countColumn != 0)
    {
        countRow = countWidget / countColumn;
        widhtWidget = rectLayout.getSize().x / countRow;
        heightWidget = rectLayout.getSize().y / countColumn;
    }
    else if(countRow != 0 && countColumn == 0)
    {
        countColumn = countWidget / countRow;
        widhtWidget = rectLayout.getSize().x / countRow;
        heightWidget = rectLayout.getSize().y / countColumn;
    }
    for (auto it = widgets.begin(); it != widgets.end(); it++)
    {
        (*it)->setSize({ widhtWidget, heightWidget});
    }

}

void Game::Widgets::Layout::setOrigin(sf::Vector2f p)
{
    rectLayout.setOrigin(p);
    auto it = widgets.begin();
    for (size_t i_row = 0; i_row < countRow; i_row++)
    {
        for (size_t i_column = 0; i_column < countColumn; i_column++)
        {
            if(it == widgets.end())
                return;
            (*it)->setOrigin({ p.x + (-widhtWidget * i_column),p.y+(-heightWidget * i_row) });
            it++;
        }
    }
}

void Game::Widgets::Layout::setPosition(sf::Vector2i p)
{
    position = p;
    for (auto it = widgets.begin(); it != widgets.end(); it++)
    {
        (*it)->setPosition(p);
    }
}

void Game::Widgets::Layout::setPositionWidget(sf::Vector2f p)
{
    rectLayout.setPosition(p);
    for (auto it = widgets.begin(); it != widgets.end(); it++)
        (*it)->setPositionWidget( p);
}

void Game::Widgets::Layout::addWidget(Game::Widgets::Widget* w)
{
	sf::Mutex m;
	m.lock();
	widgets.push_back(w);
    size_t countWidget =  widgets.size();

	if (countRow != 0 && countColumn != 0 && countRow * countColumn >= countWidget)
	{
        widhtWidget = rectLayout.getSize().x / countColumn;
        heightWidget = rectLayout.getSize().y / countRow;
	}
	else if(countRow == 0 && countColumn == 0)
	{
		if (countWidget % 2 != 0)
			countWidget += 1;
		countColumn = countWidget / 2;
		countRow = countWidget - countColumn;
        widhtWidget = rectLayout.getSize().x / countRow;
        heightWidget = rectLayout.getSize().y / countColumn;
	}
    else if(countRow == 0 && countColumn != 0)
    {
        countRow = countWidget / countColumn;
        widhtWidget = rectLayout.getSize().x / countRow;
        heightWidget = rectLayout.getSize().y / countColumn;
    }
    else if(countRow != 0 && countColumn == 0)
    {
        countColumn = countWidget / countRow;
        widhtWidget = rectLayout.getSize().x / countRow;
        heightWidget = rectLayout.getSize().y / countColumn;
    }
    auto it = widgets.begin();
    for (size_t i_row = 0; i_row < countRow; i_row++)
    {
        for (size_t i_column = 0; i_column < countColumn; i_column++)
        {
            if (it != widgets.end()) {
                (*it)->setPosition(position);
                (*it)->setPositionWidget(sf::Vector2f(position));
                (*it)->setSize({ widhtWidget, heightWidget });
                (*it)->setOrigin({rectLayout.getOrigin().x -widhtWidget * i_column,rectLayout.getOrigin().y -heightWidget * i_row });
                it++;
            }
        }
    }
	m.unlock();
}
