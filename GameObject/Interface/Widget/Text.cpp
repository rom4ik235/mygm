#include "Text.h"

Game::Widgets::Text::Text(sf::Vector2f p, std::string t, sf::Color c, int st, sf::Text::Style s)
{
    if(!font.loadFromFile(static_cast<std::string>(DATA_PATH) + "Font/arial.ttf")){
       std::cout << "Error" ;
    }
    text.setFont(font);
    text.setCharacterSize(static_cast<unsigned>(st));
    text.setStyle(s);
    text.setFillColor(c);
    text.setString(t);
    text.setPosition(p);
    failOrigin.x = text.getGlobalBounds().left - text.getPosition().x;
    failOrigin.y = text.getGlobalBounds().top - text.getPosition().y;
    text.setOrigin(text.getGlobalBounds().left-text.getPosition().x,text.getGlobalBounds().top-text.getPosition().y);
    colorText = c;
    position = static_cast<sf::Vector2i> (p);
}

Game::Widgets::Text::~Text()
{
}

void Game::Widgets::Text::update(sf::Event event)
{
}

void Game::Widgets::Text::draw(sf::RenderWindow* w)
{
    text.setFillColor(colorText);
    w->draw(text);
}

void Game::Widgets::Text::move(sf::Vector2f s)
{
    text.move(s);
}

void Game::Widgets::Text::setPositionWidget(sf::Vector2f p)
{
    text.setPosition(p);
}

void Game::Widgets::Text::setSize(sf::Vector2f s)
{
}

void Game::Widgets::Text::setOrigin(sf::Vector2f o)
{
    text.setOrigin(failOrigin.x-o.x,failOrigin.y-o.y);
}
