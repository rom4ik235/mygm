#include "Button.h"

Game::Widgets::Button::Button(std::string t, sf::Vector2f p, sf::Vector2f s, sf::Color cb, sf::Color ct, unsigned int st, float b, float i, sf::Color cbrd)
{
    position = static_cast<sf::Vector2i> (p);
    border = b;
    indent = i;
    if(!font.loadFromFile(static_cast<std::string>(DATA_PATH) + "Font/arial.ttf")){
       std::cout << "Error" ;
    }

    textButton.setFont(font);
    textButton.setCharacterSize(st);
    textButton.setStyle(sf::Text::Style::Regular);
    textButton.setFillColor(ct);
    textButton.setString(t);
    sf::FloatRect rect = textButton.getGlobalBounds();
    if(s == sf::Vector2f())
        s = sf::Vector2f(rect.width+(border*2)+indent,rect.height+(border*2)+indent);
    failTextX = textButton.getGlobalBounds().left - textButton.getPosition().x;
    failTextY = textButton.getGlobalBounds().top - textButton.getPosition().y;
    textButton.setOrigin(sf::Vector2f(((rect.width / 2) - (s.x / 2) +failTextX), ((rect.height / 2) - (s.y / 2)+failTextY)));
    textButton.setPosition(p);

    rectBorder.setSize(s);
    rectBorder.setFillColor(cbrd);
    rectBorder.setPosition(p);

    rectButton.setFillColor(cb);
    rectButton.setSize({s.x-(border*2),s.y-(border*2)});
    rectButton.setOrigin(-border,-border);
    rectButton.setPosition(p);

}

Game::Widgets::Button::Button(Game::Widgets::ButtonParam b)
{
    position = static_cast<sf::Vector2i> (b.position);
    border = b.border;
    indent = b.indent;
    if(!font.loadFromFile(static_cast<std::string>(DATA_PATH) + "Font/arial.ttf")){
       std::cout << "Error" ;
    }

    textButton.setFont(font);
    textButton.setCharacterSize(b.sizeText);
    textButton.setStyle(sf::Text::Style::Regular);
    textButton.setFillColor(b.colorText);
    textButton.setString(b.text);
    sf::FloatRect rect = textButton.getGlobalBounds();
    if(b.size == sf::Vector2f())
        b.size = sf::Vector2f(rect.width+(border*2)+indent,rect.height+(border*2)+indent);
    failTextX = textButton.getGlobalBounds().left - textButton.getPosition().x;
    failTextY = textButton.getGlobalBounds().top - textButton.getPosition().y;
    textButton.setOrigin(sf::Vector2f(((rect.width / 2) - (b.size.x / 2) +failTextX), ((rect.height / 2) - (b.size.y / 2)+failTextY)));
    textButton.setPosition(b.position);

    rectBorder.setSize(b.size);
    rectBorder.setFillColor(b.colorBorder);
    rectBorder.setPosition(b.position);

    rectButton.setFillColor(b.colorButtom);
    rectButton.setSize({b.size.x-(border*2),b.size.y-(border*2)});
    rectButton.setOrigin(-border,-border);
    rectButton.setPosition(b.position);
}

Game::Widgets::Button::~Button()
{
}

void Game::Widgets::Button::update(sf::Event event)
{
    if(flagPress){
        if(clokClick.getElapsedTime().asMilliseconds()>100){
            flagPress = false;
        }
    }
    if(event.type == sf::Event::MouseButtonReleased){
        if (rectButton.getGlobalBounds().contains(Game::getPositionMouse())){
            if(!flagPress&&event.mouseButton.button == sf::Mouse::Left){
                flagPress = true;
                if(onClick!=nullptr){
                    onClick->launch();
                    clokClick.restart();
                }
            }
        }
	}

}

void Game::Widgets::Button::draw(sf::RenderWindow* w)
{
    w->draw(rectBorder);
    w->draw(rectButton);
    w->draw(textButton);
}

void Game::Widgets::Button::move(sf::Vector2f s)
{
    rectBorder.move(s);
    rectButton.move(s);
    textButton.move(s);
}

std::string Game::Widgets::Button::getText()
{
    return textButton.getString().toAnsiString();
}

void Game::Widgets::Button::setText(std::string t, unsigned size)
{
    textButton.setString(t);
    if(size > 0)
        textButton.setCharacterSize(size);
    sf::FloatRect rect = textButton.getGlobalBounds();
    while(rect.width > rectButton.getSize().x - indent && textButton.getCharacterSize()>4){
        textButton.setCharacterSize(textButton.getCharacterSize()-1);
        rect = textButton.getGlobalBounds();
    }
    textButton.setOrigin(sf::Vector2f(((rect.width / 2) - (rectButton.getSize().x / 2)), ((rect.height / 2) - (rectButton.getSize().y / 2)) + failTextY + indent));
}

void Game::Widgets::Button::setColorBorder(sf::Color c)
{
    rectBorder.setFillColor(c);
}

void Game::Widgets::Button::setColorText(sf::Color c)
{
    textButton.setFillColor(c);
}

void Game::Widgets::Button::setColorBackGround(sf::Color c)
{
    rectButton.setFillColor(c);
}

void Game::Widgets::Button::setPositionWidget(sf::Vector2f p)
{
    rectButton.setPosition(p);
    rectBorder.setPosition(p);
    textButton.setPosition(p);
}

void Game::Widgets::Button::setSize(sf::Vector2f s)
{
    rectBorder.setSize(s);
    rectButton.setSize({s.x - (border*2), s.y - (border*2)});
    sf::FloatRect rect = textButton.getGlobalBounds();
    while(rect.width > rectButton.getSize().x - indent && textButton.getCharacterSize()>4){
        textButton.setCharacterSize(textButton.getCharacterSize()-1);
        rect = textButton.getGlobalBounds();
    }
    textButton.setOrigin(sf::Vector2f(((rect.width / 2) - (rectButton.getSize().x / 2)), ((rect.height / 2) - (rectButton.getSize().y / 2)) + failTextY + indent));
}

void Game::Widgets::Button::setOrigin(sf::Vector2f p)
{
    rectBorder.setOrigin(p.x, p.y );
    rectButton.setOrigin(p.x-border, p.y-border);
    sf::FloatRect rect = textButton.getGlobalBounds();
    textButton.setOrigin(sf::Vector2f(p.x +((rect.width / 2) - (rectButton.getSize().x / 2) +failTextX),
                                      p.y +((rect.height / 2) - (rectButton.getSize().y / 2)+failTextY)));
}
