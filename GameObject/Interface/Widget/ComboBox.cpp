#include "ComboBox.h"

Game::Widgets::ComboBox::ComboBox(sf::Vector2f p, float wL, float hL, float wB)
{
    position = static_cast<sf::Vector2i> (p);

    if(!font.loadFromFile(static_cast<std::string>(DATA_PATH) + "Font/arial.ttf")){
       std::cout << "Error" ;
    }

    textRow.setFont(font);
    textRow.setCharacterSize(20);
    textRow.setStyle(sf::Text::Style::Regular);
    textRow.setFillColor(sf::Color::Black);
    textRow.setString("");
    textRow.setOrigin(-3,-3);
    textRow.setPosition(p);

    rectBorder.setSize({wL,hL});
    rectBorder.setFillColor(sf::Color::Black);
    rectBorder.setPosition(p);

    rectLayout.setSize({wL - wB - 9,hL-6});
    rectLayout.setFillColor(sf::Color(201, 197, 197));
    rectLayout.setOrigin(-3,-3);
    rectLayout.setPosition(p);

    rectButtonUP.setSize({wB,(hL-3)/2});
    rectButtonUP.setFillColor(sf::Color(120, 66, 66));
    rectButtonUP.setOrigin(-(wL - wB - 3),-3);
    rectButtonUP.setPosition(p);

    rectButtonDOWN.setSize({wB,(hL-3)/2});
    rectButtonDOWN.setFillColor(sf::Color(66, 66, 120));
    rectButtonDOWN.setOrigin(-(wL - wB - 3),-(hL-3)/2);
    rectButtonDOWN.setPosition(p);
}

Game::Widgets::ComboBox::~ComboBox()
{

}

void Game::Widgets::ComboBox::update(sf::Event event)
{
    sf::Mutex m;
    m.lock();
    if(setList!=nullptr){
        list = setList();
        if(list == std::vector<std::string>())
        {
            list.push_back("");
            return;
        }
        else {
            textRow.setString(list[initialValue]);
        }
    }
    m.unlock();
    if (event.type == sf::Event::MouseButtonReleased &&
            event.mouseButton.button==sf::Mouse::Left){
        if (rectButtonUP.getGlobalBounds().contains(Game::getPositionMouse()))
        {
            initialValue++;
            if(initialValue>list.size()-1){
                initialValue = 0;
            }
            textRow.setString(list[initialValue]);
        }
        else if (rectButtonDOWN.getGlobalBounds().contains(Game::getPositionMouse())) {
            if(initialValue == 0)
                initialValue = list.size()-1;
            else
                initialValue--;
            textRow.setString(list[initialValue]);
        }
    }
}

void Game::Widgets::ComboBox::draw(sf::RenderWindow *w)
{
    w->draw(rectBorder);
    w->draw(rectLayout);
    w->draw(rectButtonUP);
    w->draw(rectButtonDOWN);
    w->draw(textRow);
}

void Game::Widgets::ComboBox::setPositionWidget(sf::Vector2f p)
{
    rectLayout.setPosition(p);
    rectBorder.setPosition(p);
    rectButtonUP.setPosition(p);
    rectButtonDOWN.setPosition(p);
    textRow.setPosition(p);
}

std::string Game::Widgets::ComboBox::getText()
{
    if(list == std::vector<std::string>())
        return "";
    return list[initialValue];
}

void Game::Widgets::ComboBox::addRowString(std::string s)
{
    list.push_back(s);
    textRow.setString(list[0]);
}

void Game::Widgets::ComboBox::setSize(sf::Vector2f s)
{
    rectBorder.setSize(s);

    rectLayout.setSize({s.x - rectButtonUP.getSize().x - 9,s.y-6});
    rectLayout.setOrigin(-3,-3);

    rectButtonUP.setSize({rectButtonUP.getSize().x ,(s.y/2)-3});
    rectButtonUP.setOrigin(-(s.x - rectButtonUP.getSize().x  - 3),-3);

    rectButtonDOWN.setSize({rectButtonUP.getSize().x ,(s.y/2)-3});
    rectButtonDOWN.setOrigin(-(s.x - rectButtonUP.getSize().x - 3),-(s.y-3)/2);
}

void Game::Widgets::ComboBox::setOrigin(sf::Vector2f p)
{
    rectBorder.setOrigin(p.x, p.y);
    rectLayout.setOrigin(p.x - 3, p.y - 3);
    rectButtonUP.setOrigin(-rectLayout.getSize().x - 6 + p.x, p.y - 3);
    rectButtonDOWN.setOrigin(-rectLayout.getSize().x - 6 + p.x, - rectButtonDOWN.getSize().y + p.y - 3);
    textRow.setOrigin(p.x - 3, p.y - 3);
}
