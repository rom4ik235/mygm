#include "TextEdit.h"

Game::Widgets::TextEdit::TextEdit(sf::Vector2f p, sf::Vector2f s, std::string t, sf::Color c, int st, sf::Text::Style sty)
{
    textString = t;
    position = static_cast<sf::Vector2i> (p);
    if(!font.loadFromFile(static_cast<std::string>(DATA_PATH) + "Font/arial.ttf")){
       std::cout << "Error" ;
    }
    rectBorder.setSize(s);
    rectBorder.setPosition(p);
    rectBorder.setFillColor(sf::Color::Black);
    rectEdit.setSize({ s.x - 6, s.y - 6 });
    rectEdit.setOrigin(-3, -3);
    rectEdit.setPosition(p);
    rectEdit.setFillColor(sf::Color::Black);

    flagPress = false;
    textEdit.setFont(font);
    textEdit.setCharacterSize(static_cast<unsigned int>(st));
    textEdit.setStyle(sty);
    textEdit.setFillColor(c);
    textEdit.setString(textString);
    textEdit.setOrigin(-3, -3);
    textEdit.setPosition(p);
}

Game::Widgets::TextEdit::~TextEdit()
{

}

void Game::Widgets::TextEdit::update(sf::Event event)
{
    if(event.type == sf::Event::MouseButtonReleased){
        if (rectEdit.getGlobalBounds().contains(Game::getPositionMouse())){
            if(event.mouseButton.button == sf::Mouse::Left){
                flagPress = !flagPress;
            }
        }
        else
            flagPress = false;
    }
    if(flagPress){
#ifdef __linux__
        if ((event.key.code == sf::Keyboard::Return && event.type == sf::Event::KeyReleased) || event.type == sf::Event::MouseLeft)
#else
        if ((event.key.code == sf::Keyboard::Return && event.type == sf::Event::KeyReleased) || event.type == sf::Event::MouseLeft)
#endif
        {
            flagPress = !flagPress;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            textString += " ";
            textEdit.setString(textString);
        }
        else if (event.key.code == sf::Keyboard::Space && event.type == sf::Event::KeyReleased) {
            return;
        }
        else if(sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace)&& textString.size()!=0)
        {
            textString.erase(textString.size() -1);
            textEdit.setString(textString);
        }
        else if((event.text.unicode >= 48 && event.text.unicode <= 57) ||
                (event.text.unicode >= 65 && event.text.unicode <= 90) ||
                (event.text.unicode >= 97 && event.text.unicode <= 122)){
            textString += static_cast<char>(event.text.unicode);
            textEdit.setString(textString);

            if(textEdit.getGlobalBounds().height>rectEdit.getSize().y){
                textString.erase(textString.size()-1);
                textString.erase(textString.size()-1);
                textEdit.setString(textString);
            }
            else if(textEdit.getGlobalBounds().width>rectEdit.getSize().x){
                textString.erase(textString.size()-1);
                textString += "\n";
                textEdit.setString(textString);
            }
        }
    }
}

void Game::Widgets::TextEdit::draw(sf::RenderWindow *w)
{
    if (flagPress)
        rectEdit.setFillColor(sf::Color(209, 209, 209));
    else
        rectEdit.setFillColor(sf::Color(156, 152, 152));
    w->draw(rectBorder);
    w->draw(rectEdit);
    w->draw(textEdit);
}

void Game::Widgets::TextEdit::setPositionWidget(sf::Vector2f p)
{
    rectBorder.setPosition(p);
    rectEdit.setPosition(p);
    textEdit.setPosition(p);
}

void Game::Widgets::TextEdit::setSize(sf::Vector2f s)
{
    rectBorder.setSize(s);

    rectEdit.setSize({ s.x - 6, s.y - 6 });
    rectEdit.setOrigin(-3, -3);
}

void Game::Widgets::TextEdit::setOrigin(sf::Vector2f p)
{
    rectBorder.setOrigin(p.x, p.y);
    rectEdit.setOrigin(p.x-3, p.y-3);
    textEdit.setOrigin(p.x-3, p.y-3);
}

std::string Game::Widgets::TextEdit::getText()
{
    return  textString;
}

void Game::Widgets::TextEdit::setStrPtr(std::string s)
{
    textString = s;
}
