#include "ScrollLayout.h"

Game::Widgets::ScrollLayout::ScrollLayout(sf::Vector2f p, float wL, float hL, float wB, float hB, size_t coutW)
{
	position = static_cast<sf::Vector2i>(p);
	coutWidget = coutW;
	rectBorder.setSize({ wL,hL });
	rectBorder.setPosition(p);
	rectBorder.setFillColor(sf::Color::Black);
	
	rectLayout.setSize({ wL - wB - 12, hL - 8});
	rectLayout.setOrigin(-4, -4);
	rectLayout.setFillColor(sf::Color(201, 197, 197));
	rectLayout.setPosition(p);

	rectScrollingField.setSize({wB,hL-8});
	rectScrollingField.setOrigin({ -(rectLayout.getSize().x+8),-4 });
	rectScrollingField.setFillColor(sf::Color(201, 197, 197));
	rectScrollingField.setPosition(p);

	rectScrollingButton.setSize({ wB,hB });
	rectScrollingButton.setOrigin({ -(rectLayout.getSize().x + 8),-4 });
	rectScrollingButton.setFillColor(sf::Color(61, 60, 60));
	rectScrollingButton.setPosition(p);
	widhtWidget = rectLayout.getSize().x;
	heightWidget = rectLayout.getSize().y / coutWidget;
}

Game::Widgets::ScrollLayout::~ScrollLayout()
{
}

void Game::Widgets::ScrollLayout::update(sf::Event event)
{
    for (size_t i = initialValue; (i - initialValue) < coutWidget; i++) {
		if (widgets.size() - 1 < i)
			break;
		widgets[i]->update(event);
	}
    if (event.type == sf::Event::MouseButtonPressed){
        if (rectScrollingButton.getGlobalBounds().contains(Game::getPositionMouse())){
			flagPress = true;
            rectScrollingButton.setFillColor(sf::Color(82, 82, 82));
        }
    }
    else if (event.type == sf::Event::MouseButtonReleased){
		flagPress = false;
        rectScrollingButton.setFillColor(sf::Color(61, 60, 60));
    }

	if (flagPress) {
		float shift = position.y - Game::getPositionMouse().y;
//        std::cout << "position.y = " <<position.y << std::endl;

        if (shift > -4 -(rectScrollingButton.getSize().y/2))
            shift = -4 - (rectScrollingButton.getSize().y/2);
        else if(shift < -(rectLayout.getSize().y + 4 - (rectScrollingButton.getSize().y/2)))
            shift = -(rectLayout.getSize().y + 4 - (rectScrollingButton.getSize().y/2));

        rectScrollingButton.setOrigin(-(rectLayout.getSize().x + 8), shift + (rectScrollingButton.getSize().y/2));

        shift = abs(static_cast<int>(shift)) / shiftScroll;
        if (static_cast<size_t>(shift) != initialValue) {
            initialValue = static_cast<size_t>(shift);
			updateLayout();
        }

	}
}

void Game::Widgets::ScrollLayout::draw(sf::RenderWindow* w)
{
	w->draw(rectBorder);
	w->draw(rectLayout);
    for (size_t i = initialValue; (i - initialValue) < coutWidget; i++) {
		if (widgets.size() - 1 < i)
            break;
		widgets[i]->draw(w);
	}
	w->draw(rectScrollingField);
	w->draw(rectScrollingButton);
}

void Game::Widgets::ScrollLayout::setPositionWidget(sf::Vector2f p)
{
    rectLayout.setPosition(p);
    rectBorder.setPosition(p);
    rectScrollingField.setPosition(p);
    rectScrollingButton.setPosition(p);
    for (size_t i = initialValue; (i - initialValue) < coutWidget; i++) {
        if (widgets.size() - 1 < i)
            break;
        widgets[i]->setPositionWidget(p);
    }
}

void Game::Widgets::ScrollLayout::updateLayout()
{
	sf::Mutex m;
	m.lock();
    for (size_t i_row = initialValue; (i_row - initialValue) < coutWidget; i_row++)
	{
		if (widgets.size() - 1 < i_row)
			break;
		widgets[i_row]->setOrigin({ -4,-(heightWidget * (i_row - initialValue)) - 4 });
	}
	m.unlock();
}

void Game::Widgets::ScrollLayout::addWidget(Game::Widgets::Widget* w)
{
	sf::Mutex m;
	m.lock();
	widgets.push_back(w);
    if(widgets.size()-1>coutWidget)
        shiftScroll = static_cast<int>((rectLayout.getSize().y + 4 - rectScrollingButton.getSize().y) / (widgets.size()-coutWidget));
    widgets[widgets.size() - 1]->setSize({ widhtWidget, heightWidget });
    widgets[widgets.size() - 1]->setPosition(position);
    widgets[widgets.size() - 1]->setOrigin({ -4,-(heightWidget * (widgets.size() - 1 - initialValue)) - 4 });
    widgets[widgets.size() - 1]->setPositionWidget(sf::Vector2f(position));
	m.unlock();
}
