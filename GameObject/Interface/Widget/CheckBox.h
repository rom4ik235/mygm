#ifndef CHECKBOX_H
#define CHECKBOX_H



#include "Widget.h"

namespace Game {
    namespace Widgets {
        class CheckBox : public Game::Widgets::Widget
        {
        public:

            CheckBox(std::string t,sf::Vector2f p, sf::Vector2f s, sf::Color cb, sf::Color ct, unsigned int st, float b = 3 , float i = 5, sf::Color cbrd = sf::Color::Black);

            ~CheckBox() override;

            void update(sf::Event event) override;

            void draw(sf::RenderWindow* w) override;

            void setPositionWidget(sf::Vector2f p) override;

            void setSize(sf::Vector2f s) override;

            void setOrigin(sf::Vector2f p) override;

            void move(sf::Vector2f s) override;

            void setText(std::string t, unsigned size = 0);

            std::string getText();

            void setColorBorder(sf::Color c);

            void setColorText(sf::Color c);

            void setColorBackGround(sf::Color c);

        private:
            sf::RectangleShape rectBorderText;
            sf::RectangleShape rectBorder;
            sf::RectangleShape rectBox;
            sf::RectangleShape rectCheck;
            sf::RectangleShape rectText;
            sf::Font font;
            sf::Text textCheck;
            sf::Color color;
            float failTextY;
            float failTextX;
            float indent = 5;
            float border = 5;
            bool flagPress = false;
        };
    }
}
#endif // CHECKBOX_H
