#ifndef COMBOBOX_H
#define COMBOBOX_H

#include "Widget.h"

namespace Game {
    namespace Widgets {
        class ComboBox: public Game::Widgets::Widget
        {
        public:
            ComboBox(sf::Vector2f p, float wL, float hL, float wB);

            ~ComboBox() override;

            void update(sf::Event event) override;

            void draw(sf::RenderWindow* w) override;

            void setPositionWidget(sf::Vector2f p) override;

            std::string getText();

            void addRowString(std::string s);

//            template<typename T>
            std::vector<std::string> (*setList)() = nullptr;

            void setSize(sf::Vector2f s) override;

            void setOrigin(sf::Vector2f p) override;

        private:
            sf::RenderWindow* window = nullptr;
            sf::RectangleShape rectLayout;
            sf::RectangleShape rectBorder;
            sf::RectangleShape rectButtonUP;
            sf::RectangleShape rectButtonDOWN;
            sf::Text textRow;
            std::vector<std::string> list = {};
            size_t initialValue = 0;
            sf::Font font;
        };
    }
}

#endif // COMBOBOX_H
