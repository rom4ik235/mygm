#ifndef TEXTEDIT_H
#define TEXTEDIT_H
#include "Widget.h"

namespace Game {
    namespace Widgets {
        class TextEdit : public Game::Widgets::Widget
        {
        public:
            TextEdit(sf::Vector2f p, sf::Vector2f s, std::string t, sf::Color c, int st, sf::Text::Style sty = sf::Text::Style::Regular);

            ~TextEdit() override;

            void update(sf::Event event) override;

            void draw(sf::RenderWindow* w) override;

            void setPositionWidget(sf::Vector2f p) override;

            void setSize(sf::Vector2f s) override;

            void setOrigin(sf::Vector2f p) override;

            std::string getText();

            void setStrPtr(std::string s);

        private:
            sf::Color colorText;
            sf::Text textEdit;
            std::string textString;
            sf::RectangleShape rectEdit;
            sf::RectangleShape rectBorder;
            bool flagPress = false;
            sf::Vector2f failOrigin;
            sf::Font font;
        };
    }
}
#endif // TEXTEDIT_H
