#ifndef WIDGET_SCROLLLAYOUT_H
#define WIDGET_SCROLLLAYOUT_H

#include "Widget.h"

namespace Game {
    namespace Widgets {
        class ScrollLayout : public Game::Widgets::Widget
        {
        public:
            ScrollLayout(sf::Vector2f p, float wL, float hL, float wB,float hB, size_t coutW = 6);

            ~ScrollLayout()override;

            void update(sf::Event event) override;

            void draw(sf::RenderWindow* w) override;

            void setPositionWidget(sf::Vector2f p) override;

            void updateLayout();

            void addWidget(Game::Widgets::Widget* w);

        private:
            sf::RectangleShape rectLayout;
            sf::RectangleShape rectBorder;
            sf::RectangleShape rectScrollingField;
            sf::RectangleShape rectScrollingButton;
            std::vector<Game::Widgets::Widget*> widgets;
            float widhtWidget = 0;
            float heightWidget = 0;
            bool flagPress = false;
            int shiftScroll = HEIGHT;
            size_t coutWidget = 0;
            size_t initialValue = 0;
        };
    }
}

#endif // WIDGET_SCROLLLAYOUT_H
