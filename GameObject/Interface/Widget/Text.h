#ifndef TEXT_WIDGET_H
#define TEXT_WIDGET_H
#include "Widget.h"

namespace Game {
    namespace Widgets {
        class Text : public Game::Widgets::Widget
        {
        public:
            Text(sf::Vector2f p, std::string t, sf::Color c,int st = 30, sf::Text::Style s = sf::Text::Style::Regular);

            ~Text() override;

            void update(sf::Event event) override;

            void draw(sf::RenderWindow* w) override;

            void setPositionWidget(sf::Vector2f p) override;

            void setSize(sf::Vector2f s) override;

            void setOrigin(sf::Vector2f o) override;

            void move(sf::Vector2f s) override;

            sf::Text text;
        private:
            sf::Color colorText;
            sf::Vector2f failOrigin;
            sf::Font font;
        };
    }
}

#endif // TEXT_WIDGET_H
