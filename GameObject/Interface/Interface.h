#ifndef INTERFACE_H
#define INTERFACE_H

#ifdef __linux__
#include "GameObject/Interface/Widget/Widget.h"
#include "GameObject/Interface/Widget/Button.h"
#include "GameObject/Interface/Widget/Text.h"
#include "GameObject/Interface/Widget/Layout.h"
#include "GameObject/Interface/Widget/TextEdit.h"
#include "GameObject/Interface/Widget/ScrollLayout.h"
#include "GameObject/Interface/Widget/ComboBox.h"
#include "GameObject/Interface/Widget/CheckBox.h"
#else
#include "Widget/Widget.h"
#include "Widget/Button.h"
#include "Widget/Text.h"
#include "Widget/Layout.h"
#include "Widget/TextEdit.h"
#include "Widget/ScrollLayout.h"
#include "Widget/ComboBox.h"
#include "Widget/CheckBox.h"
#endif // Qt


namespace Game {
	class Interface
	{
	public:

		Interface(sf::RenderWindow* w);

        void update(sf::Event event);

		void draw();

		void thredInterface();

		template<class T>
		long int addWidgetToListMap(T obj);

        bool removeWidget(long int i);

        bool clearListWidget();


		void arrangeWidgetOnGrid(Game::Widgets::Widget* w, sf::Vector2i sizeGrid, sf::Vector2i pos, sf::Vector2i size);

	private:
        sf::CircleShape cursor;
		sf::Vector2f posMouse;
		sf::RenderWindow* window;
		std::map<long int, Game::Widgets::Widget*> list_widget;
	};

    Game::Interface *getInterface();

	template<class T>
	long int Interface::addWidgetToListMap(T obj)
	{
		for (long int i = 0; true; i++) {
			if (Game::Interface::list_widget[i] == nullptr) {
				Game::Interface::list_widget[i] = obj;
				return i;
			}
		}
		return -1;
	}

}

#endif // INTERFACE_H
