
#ifdef __linux__
#include "GameObject/player.h"
#else
#include "player.h"
#endif // Qt

//Game::GameObjects::Player::Player(Game::GameObjects::TypeObjects::TypeObject *r,sf::RenderWindow *w){
//    camera.setSize(WIDHT, HEIGHT);
//    type = r;
//    nameObj = "Player";
//    areaHit.setFillColor(sf::Color(255,0,0,50));
//    areaHit.setRadius(r->areaHitRadius);
//    areaHit.setOrigin(r->areaHitRadius,r->areaHitRadius);
//    areaHit.setPosition(r->getPos());

//    location.setSize(sf::Vector2f(150,150));
//    location.setFillColor(sf::Color(255,255,0,20));
//    location.setPosition(r->getPos());
//    location.setOrigin(75,75);

//    win = w;
//}

//bool Game::GameObjects::Player::update()
//{
//    sf::Vector2f p = win->mapPixelToCoords( sf::Mouse::getPosition(*win));
//    speed=(sf::Vector2f(0,0));
//    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)){speed.y=-1;}
//    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)){speed.y=+1;}
//    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)){speed.x=-1;}
//    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)){speed.x=+1;}
//    if(sf::Mouse::isButtonPressed(sf::Mouse::Right)){
//        if(type->getAnim()!='A'){
//            type->attackRight(sf::Vector2f(p.x,p.y));
//        }
//    }
//    if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
//        if(type->getAnim()!='A'){
//            type->attackLeft(sf::Vector2f(p.x,p.y));
//        }
//    }
//    if(Game::GameObjects::StaticObjects::checkContactStatick({location.getPosition().x+(speed.x*areaHit.getRadius())
//                                                          ,location.getPosition().y+(speed.y*areaHit.getRadius())}))
//    {
//        speed = {0,0};
//    }
//    speed = {speed.x * type->gain,speed.y * type->gain};
//    type->move(speed);
//    location.move(speed);
//    areaHit.move(speed);
//    if(widget!=nullptr){
//        widget->move(speed);
//    }
//    return 0;
//}

//void Game::GameObjects::Player::draw(sf::RenderWindow *w){
//    camera.setCenter(location.getPosition());
//    type->draw(w);
//    w->setView(camera);
//    if(widget!=nullptr){
//        widget->draw(w);
//    }
//}

//inline bool Game::GameObjects::Player::checkContactObject(long int i){

////    if(checkObj->nameObj=="Enemy"){
////        if(anim=='A'){
////            float alfa = ((spriteAxe.getRotation()-90)/180)*3.14159265f;
////            float x = (cosf(alfa)*30)+location.getPosition().x;
////            float y = (sinf(alfa)*30)+location.getPosition().y;
////            if(checkObj->getRect().contains(x,y)){
////                if(checkObj->getAnim()!='B'){
////                    checkObj->setSpeed(sf::Vector2f(cosf(alfa)*30,sinf(alfa)*30));
////                    checkObj->setAnim('D');
////                }
////                animationCount=20;
////            }
////        }
////    }
//    return 0;
//}

//bool Game::GameObjects::Player::animation(){
//    return 0;
//}

//void Game::GameObjects::Player::setWidget(Game::Widgets::Widget *w)
//{
//    widget = w;
//}

Game::Player::Player()
{
    camera.setSize(WIDHT, HEIGHT);
}

void Game::Player::attachObject(long int i)
{
    if(playerObject==nullptr){
        playerObject = (*Game::GameObjects::getDinamicObjects())[i];
        index = i;
    }
}

long int Game::Player::getIndexPlayer()
{
    return index;
}

minibson::document Game::Player::update()
{
    sf::Vector2f speed=(sf::Vector2f(0,0));
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)){speed.y=-1;}
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)){speed.y=+1;}
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)){speed.x=-1;}
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)){speed.x=+1;}

    if(playerObject==nullptr)
    {
        camera.move(speed);
        return minibson::document();
    }
    else {
        minibson::document doc;
        if(speed == sf::Vector2f(0,0))
        {
            doc.set("anim",static_cast<int>(Game::GameObjects::AnimationEnum::Inaction));
        }
        else {
            doc.set("anim",static_cast<int>(Game::GameObjects::AnimationEnum::Move));
        }
        minibson::document pos;
        pos.set("x",static_cast<double>(playerObject->getPos().x+speed.x));
        pos.set("y",static_cast<double>(playerObject->getPos().y+speed.y));
        doc.set("pos",pos);
        camera.setCenter(playerObject->getPos());
        return doc;
    }
}

void Game::Player::draw(sf::RenderWindow *_window)
{
    _window->setView(camera);
}
