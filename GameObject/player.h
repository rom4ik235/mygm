#ifndef GAME_PLAYER_H
#define GAME_PLAYER_H

#ifdef  __linux__
#include "game.h"
#else
#include "D:\WorkSpace\mygm\game.h"
#endif

namespace Game {
    class Player
    {
    public:
        Player();
        void eventPull();
        void attachObject(long int index);
        std::vector<sf::Event> pushEvent();
        long int getIndexPlayer();
        minibson::document update();
        void draw(sf::RenderWindow *_window);

////            bool checkContactObject(long int i) override;

//            bool animation();

//            bool move(sf::Vector2f _pos);

//            void setWidget(Game::Widgets::Widget *w);

    private:
        Game::GameObjects::DinamicObject* playerObject = nullptr;
        sf::View camera;
        long int index =-1;
//            sf::RenderWindow *win;
//            Game::Widgets::Widget *widget = nullptr;
//            Game::GameObjects::Weapons::Weapon *Weapon1 = nullptr;
//            Game::GameObjects::Weapons::Weapon *Weapon2 = nullptr;
    };
}

#endif // GAME_PLAYER_H
