#ifndef BUSH_H
#define BUSH_H

#include "staticobject.h"
namespace Game {
    namespace GameObjects {
        namespace  StaticObjects{
            class Bush : public StaticObject
            {
            public:
                Bush(float px, float py, int rotation = 0);
                bool update() override;
                void draw(sf::RenderWindow *w) override;
//                sf::Vertex checkContactObject(sf::Vector2f pl, sf::Vertex *pM, float r) ;
            private:
                sf::Texture textureBush;
            };
        }
    }
}
#endif // BUSH_H
