#include "staticobject.h"

Game::GameObjects::StaticObject::StaticObject(float px, float py)
{

}

Game::GameObjects::StaticObject::~StaticObject()
{

}

bool Game::GameObjects::StaticObject::update()
{
    return 0;
}

void Game::GameObjects::StaticObject::draw(sf::RenderWindow *_window)
{

}

sf::FloatRect Game::GameObjects::StaticObject::getRect()
{
    return rect.getGlobalBounds();
}

sf::FloatRect Game::GameObjects::StaticObject::getRectBake(int r)
{
    sf::FloatRect new_rect = rect.getGlobalBounds();
    return sf::FloatRect(new_rect.left - r, new_rect.top - r, new_rect.width + r*2, new_rect.height + r*2);
}

sf::VertexArray Game::GameObjects::StaticObject::getPoint()
{
    sf::FloatRect r = getRect();
    sf::VertexArray point(sf::Points, 4);
    point[0]=sf::Vertex({r.left, r.top},sf::Color::Yellow);
    point[1]=sf::Vertex({r.left + r.width, r.top},sf::Color::Yellow);
    point[2]=sf::Vertex({r.left + r.width, r.top + r.height},sf::Color::Yellow);
    point[3]=sf::Vertex({r.left, r.top + r.height},sf::Color::Yellow);
    return point;
}

sf::VertexArray Game::GameObjects::StaticObject::getPointBake(int radius)
{
    sf::FloatRect r = getRectBake(radius);
    sf::VertexArray point(sf::Points, 4);
    point[0]=sf::Vertex({r.left, r.top},sf::Color::Yellow);
    point[1]=sf::Vertex({r.left + r.width, r.top},sf::Color::Yellow);
    point[2]=sf::Vertex({r.left + r.width, r.top + r.height},sf::Color::Yellow);
    point[3]=sf::Vertex({r.left, r.top + r.height},sf::Color::Yellow);
//    point[4]=sf::Vertex({r.left, r.top},sf::Color::Yellow);
    return point;
}


static std::map<long int,Game::GameObjects::StaticObject*> list_static_objects;
static std::vector<sf::FloatRect> array_static_objects;

std::map<long, Game::GameObjects::StaticObject *> *Game::GameObjects::getListStaticObject()
{
    return &list_static_objects;
}

std::vector<sf::FloatRect> *Game::GameObjects::getArrayRectBake()
{
    return &array_static_objects;
}


void Game::GameObjects::clearStaticObject() {
    sf::Mutex m;
    m.lock();
    list_static_objects.clear();
    m.unlock();
}

void Game::GameObjects::BakeStaticObject()
{
    sf::Mutex m;
    m.lock();
    array_static_objects.clear();
    for (auto it = list_static_objects.begin();it != list_static_objects.end();it++) {
        array_static_objects.push_back(it->second->getRect());
    }
    m.unlock();
}


bool Game::GameObjects::checkContactStatick(sf::Vector2f p)
{
    if(array_static_objects.size()==0){
        return 0;
    }
    for (auto it = array_static_objects.begin();it != array_static_objects.end();it++) {
        if(it->contains(p))
            return 1;
    }
    return 0;
}

bool check(sf::Vector2f p){
    if(array_static_objects.size()==0){
        return 0;
    }
    for (auto it = array_static_objects.begin();it != array_static_objects.end();it++) {
        if(it->contains(p))
            return 1;
    }
    return 0;
}

static bool dir = 0;

sf::VertexArray Game::GameObjects::wayToThePoint(sf::Vector2f pl,sf::Vector2f pm, int r)
{
    sf::VertexArray point(sf::LineStrip);
    point.append(sf::Vertex(pl,sf::Color::Red));
    if(check(pm))
        return point;
    while (true) {
        size_t i = point.getVertexCount()-1;
        float x = point[i].position.x-pm.x;
        float y = point[i].position.y-pm.y;
        float len = sqrtf(powf(x,2)+powf(y,2));
        x = (x*r)/len;
        y = (y*r)/len;
        if(len < r){
            return point;
        }

        sf::Vector2f p = {point[i].position.x-x,point[i].position.y-y};
        if(!check(p)){
           point.append(sf::Vertex(p,sf::Color::Red));
           continue;
        }
        if(dir){
            p = {point[i].position.x+y,point[i].position.y-x};
        }
        else {
            p = {point[i].position.x-y,point[i].position.y+x};
        }
        if (!check(p)){
            point.append(sf::Vertex(p,sf::Color::Red));
            continue;
        }
        dir = !dir;
        if(point.getVertexCount()<100){
            point.clear();
            point.append(sf::Vertex(pl,sf::Color::Red));
            return point;
        }
        continue;
    }
}


