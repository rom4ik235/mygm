#include "bush.h"

Game::GameObjects::StaticObjects::Bush::Bush(float px, float py, int rotation):StaticObject(px,py)
{
    if (!textureBush.loadFromFile(static_cast<std::string>(DATA_PATH) + "sprite/bush1_200x100.png"))
        std::cout << "Error" ;
    rect.setTexture(&textureBush);
    rect.setSize(sf::Vector2f(200,100));
    rect.setOrigin(100,50);
    rect.setPosition({px,py});
    rect.setRotation(rotation);
    rect.setScale(0.5,0.5);

}

bool Game::GameObjects::StaticObjects::Bush::update()
{
    return 0;
}

void Game::GameObjects::StaticObjects::Bush::draw(sf::RenderWindow *w)
{
    w->draw(rect);
}

//sf::Vertex Game::GameObjects::StaticObjects::Bush::checkContactObject(sf::Vector2f pl, sf::Vertex *pM, float r)
//{
//    sf::Vertex m = *pM;
//    if(rect.getGlobalBounds().contains(m.position.x+r,m.position.y+r) ||
//            rect.getGlobalBounds().contains(m.position.x-r,m.position.y-r))
//    {
//        m.position.x = m.position.x + (m.position.x - rect.getPosition().x);
//        m.position.y = m.position.y + (m.position.y - rect.getPosition().y);
//        *pM = m;
//    }
//    if(rect.getGlobalBounds().contains(pl.x,pl.y-r)){
//        r = r + 5;
//        float x1 = (rect.getPosition().x+(rect.getSize().x/2)+r);
//        float y1 = (rect.getPosition().y+(rect.getSize().y/2)+r);
//        float x2 = (rect.getPosition().x-(rect.getSize().x/2)-r);
//        float y2 = (rect.getPosition().y+(rect.getSize().y/2)+r);
//        if(fabs(static_cast<double>(x1-m.position.x)) < fabs(static_cast<double>(x2-m.position.x))){
//            return sf::Vertex(sf::Vertex(sf::Vector2f(x1,y1),sf::Color::Red));
//        }
//        else {
//            return sf::Vertex(sf::Vertex(sf::Vector2f(x2,y2),sf::Color::Red));
//        }
//    }
//    if(rect.getGlobalBounds().contains(pl.x,pl.y+r)){
//        r = r + 5;
//        float x1 = (rect.getPosition().x+(rect.getSize().x/2)+r);
//        float y1 = (rect.getPosition().y-(rect.getSize().y/2)-r);
//        float x2 = (rect.getPosition().x-(rect.getSize().x/2)-r);
//        float y2 = (rect.getPosition().y-(rect.getSize().y/2)-r);
//        if(fabs(static_cast<double>(x1-m.position.x)) < fabs(static_cast<double>(x2-m.position.x))){
//            return sf::Vertex(sf::Vertex(sf::Vector2f(x1,y1),sf::Color::Red));
//        }
//        else {
//            return sf::Vertex(sf::Vertex(sf::Vector2f(x2,y2),sf::Color::Red));
//        }
//    }
//    if(rect.getGlobalBounds().contains(pl.x-r,pl.y)){
//        r = r + 5;
//        float x1 = (rect.getPosition().x+(rect.getSize().x/2)+r);
//        float y1 = (rect.getPosition().y-(rect.getSize().y/2)-r);
//        float x2 = (rect.getPosition().x+(rect.getSize().x/2)+r);
//        float y2 = (rect.getPosition().y+(rect.getSize().y/2)+r);
//        if(fabs(static_cast<double>(y1-m.position.y)) < fabs(static_cast<double>(y2-m.position.y))){
//            return sf::Vertex(sf::Vertex(sf::Vector2f(x1,y1),sf::Color::Red));
//        }
//        else {
//            return sf::Vertex(sf::Vertex(sf::Vector2f(x2,y2),sf::Color::Red));
//        }
//    }
//    if(rect.getGlobalBounds().contains(pl.x+r,pl.y)){
//        r = r + 5;
//        float x1 = (rect.getPosition().x-(rect.getSize().x/2)-r);
//        float y1 = (rect.getPosition().y-(rect.getSize().y/2)-r);
//        float x2 = (rect.getPosition().x-(rect.getSize().x/2)-r);
//        float y2 = (rect.getPosition().y+(rect.getSize().y/2)+r);
//        if(fabs(static_cast<double>(y1-m.position.y)) < fabs(static_cast<double>(y2-m.position.y))){
//            return sf::Vertex(sf::Vertex(sf::Vector2f(x1,y1),sf::Color::Red));
//        }
//        else {
//            return sf::Vertex(sf::Vertex(sf::Vector2f(x2,y2),sf::Color::Red));
//        }
//    }
//    return sf::Vertex();
//}
