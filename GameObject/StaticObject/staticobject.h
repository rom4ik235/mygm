#ifndef STATICOBJECT_H
#define STATICOBJECT_H

#ifdef __linux__
#include "variable_storage.h"
#else
#include "D:/WorkSpace/mygm/variable_storage.h"
#endif 

namespace Game {
    namespace GameObjects {
        class StaticObject
        {
        public:
            StaticObject(float px,float py);
            virtual ~StaticObject();
            virtual bool update();
            virtual void draw(sf::RenderWindow *_window);
            sf::FloatRect getRect();
            sf::FloatRect getRectBake(int r);
            sf::VertexArray getPoint();
            sf::VertexArray getPointBake(int r);

        protected:
            sf::RectangleShape rect;
        };

        std::map<long, Game::GameObjects::StaticObject *> *getListStaticObject();
        std::vector<sf::FloatRect> *getArrayRectBake();
        void BakeStaticObject();

        template<class T>
        long int addStaticObjectToListMap(T obj){
            for (long int i=0;true;i++) {
                if((*getListStaticObject())[i]==nullptr){
                    (*getListStaticObject())[i] = obj;
                    BakeStaticObject();
                    return i;
                }
            }

        }
        bool checkContactStatick(sf::Vector2f p);
        sf::VertexArray wayToThePoint(sf::Vector2f pl,sf::Vector2f pm, int r);
        void clearStaticObject();
    }
}

#endif // STATICOBJECT_H
