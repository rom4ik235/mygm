
#ifdef __linux__
#include "GameObject/gameobject.h"
#else
#include  "D:/WorkSpace/mygm/GameObject/gameobject.h"
#endif // Qt

//Game::GameObjects::DinamicObject::DinamicObject(/*const char* name) : nameObj(name*/) /*: nameObj("213")*/
//{
//    rectLocation.setSize({200,200});
//}

Game::GameObjects::DinamicObject::~DinamicObject() {}

bool Game::GameObjects::DinamicObject::update(minibson::document property)
{
	rectLocation.setPosition(static_cast<float>(property.get("pos", minibson::document()).get("x", 0.0)),
		static_cast<float>(property.get("pos", minibson::document()).get("y", 0.0)));
	return 0;
}

minibson::document Game::GameObjects::DinamicObject::update()
{
	return minibson::document();
}

void Game::GameObjects::DinamicObject::draw(sf::RenderWindow* _window) {
	_window->draw(rectLocation);
}

void Game::GameObjects::DinamicObject::contactObject(minibson::document property)
{
	property.get("Hello " + std::string(nameObj), "");
}

bool Game::GameObjects::DinamicObject::checkContactObject(long i)
{
	contactObject(minibson::document().set("Hello " + std::string((*getDinamicObjects())[i]->nameObj),
		"I " + std::string(nameObj)));
	return 0;
}

float Game::GameObjects::DinamicObject::getRadiusAreaHit()
{
	return areaHit.getRadius();
}

sf::FloatRect Game::GameObjects::DinamicObject::getRect()
{
	return rectLocation.getGlobalBounds();
}

sf::Vector2f Game::GameObjects::DinamicObject::getPos()
{
	return rectLocation.getPosition();
}




static std::map<long int, Game::GameObjects::DinamicObject*> listDinamicObject;
static std::map<long int, sf::FloatRect> arrayRectLocation;
static sf::RenderWindow* windowGame = nullptr;

std::map<long int, Game::GameObjects::DinamicObject*>* Game::GameObjects::getDinamicObjects()
{
	return &listDinamicObject;

}

std::map<long int, sf::FloatRect>* Game::GameObjects::getArrayRectLocation()
{
	return &arrayRectLocation;
}

sf::RenderWindow* Game::GameObjects::getWindow()
{
	if (windowGame != nullptr)
		return windowGame;
	return nullptr;
}

void Game::GameObjects::setWindow(sf::RenderWindow* W)
{
	if (windowGame == nullptr)
		windowGame = W;
}

//static bool flag_thread = true;

void Game::GameObjects::CheckContact() {
	//while(flag_thread){
	for (auto it = arrayRectLocation.begin(); it != arrayRectLocation.end(); it++) {
		for (auto it1 = arrayRectLocation.begin(); it1 != arrayRectLocation.end(); it1++) {
			if (it->first == it1->first)
				continue;
			if (arrayRectLocation[it->first].intersects(it1->second)) {
				listDinamicObject[it->first]->checkContactObject(it1->first);
			}
		}
	}
	//}
}

//static sf::Thread checkThr(threadCheck);

void Game::GameObjects::clearObjectToListMap() {
	sf::Mutex m;
	m.lock();
	arrayRectLocation.clear();
	listDinamicObject.clear();
	m.unlock();
}
