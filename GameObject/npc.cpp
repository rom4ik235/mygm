#ifdef __linux__
#include "GameObject/npc.h"
#include "GameObject/StaticObject/staticobject.h"
#else
#include "npc.h"
#include "StaticObject/staticobject.h"
#endif // Qt
#include <cstdlib>

Game::GameObjects::NPC::NPC(Game::GameObjects::TypeObjects::TypeObject *r):/*Game::GameObjects::DinamicObject (r->nameRace) ,*/type(r)
{
    nameObj = "NPC";
    areaHit.setFillColor(sf::Color(255,0,0,20));
    areaHit.setRadius(25);
    areaHit.setOrigin(25,25);
    areaHit.setPosition(type->getPos());

    rectLocation.setSize(sf::Vector2f(400,400));
    rectLocation.setFillColor(sf::Color(255,255,0,20));
    rectLocation.setPosition(type->getPos());
    rectLocation.setOrigin(200,200);
    speed={0,0};
    pointsMove.setPrimitiveType(sf::LinesStrip);
    pointsMove.resize(6);
    pointsMove.clear();
}

bool Game::GameObjects::NPC::update(minibson::document property)
{
//    std::cout << "\nx = " <<  static_cast<float>(property.get("pos",minibson::document()).get("x",0.0))<< std::endl;
//    std::cout << "y = " <<  static_cast<float>(property.get("pos",minibson::document()).get("y",0.0))<< std::endl;
//    std::cout << "anim = " <<  static_cast<Game::GameObjects::AnimationEnum>(property.get("anim",0)) << std::endl;
//    std::cout << "test = " <<  (property.get("player","notWork"))<< "\n" << std::endl;
    sf::Vector2f p(static_cast<float>(property.get("pos",minibson::document()).get("x",0.0)),
                   static_cast<float>(property.get("pos",minibson::document()).get("y",0.0)));
    areaHit.setPosition(p);
    rectLocation.setPosition(p);
    type->setPos(p);
    anim = static_cast<Game::GameObjects::AnimationEnum>(property.get("anim",0));
    return 0;
}

minibson::document Game::GameObjects::NPC::update()
{
    sf::Mutex m;
    speed = {0,0};
    if(pointsMove.getVertexCount()>0){
        float x = pointsMove[0].position.x-rectLocation.getPosition().x;
        float y = pointsMove[0].position.y-rectLocation.getPosition().y;
        float len = sqrtf(powf(x,2)+powf(y,2));
        if(len < 2){
            m.lock();
            std::size_t count = pointsMove.getVertexCount();
            for (size_t i = 0;i<count-1;i++) {
                pointsMove[i]=pointsMove[i+1];
            }
            pointsMove.resize(count-1);
            m.unlock();
        }
        else {
            speed.x=(x*type->gain)/len;
            speed.y=(y*type->gain)/len;
        }
    }
    minibson::document doc;
    minibson::document pos;

    pos.set("x", static_cast<double>(rectLocation.getPosition().x+speed.x));
    pos.set("y", static_cast<double>(rectLocation.getPosition().y+speed.y));
    doc.set("pos",pos);
    doc.set("anim",static_cast<int>(anim));
    return doc;
}

void Game::GameObjects::NPC::draw(sf::RenderWindow *w){
    w->draw(areaHit);
    w->draw(rectLocation);
    if (pointsMove.getVertexCount() != 0)
    {
        w->draw(pointsMove);
    }
    type->draw(w);
    if(widget!=nullptr){
        widget->draw(w);
    }
}

bool Game::GameObjects::NPC::move()
{
    sf::Mutex m;
    speed = {0,0};
    if(pointsMove.getVertexCount()>0){
        float x = pointsMove[0].position.x-rectLocation.getPosition().x;
        float y = pointsMove[0].position.y-rectLocation.getPosition().y;
        float len = sqrtf(powf(x,2)+powf(y,2));
        if(len < 2){
            m.lock();
            std::size_t count = pointsMove.getVertexCount();
            for (size_t i = 0;i<count-1;i++) {
                pointsMove[i]=pointsMove[i+1];
            }
            pointsMove.resize(count-1);
            m.unlock();
        }
        else {
            speed.x=(x*type->gain)/len;
            speed.y=(y*type->gain)/len;
        }
    }
    rectLocation.move(speed);
    areaHit.move(speed);
    type->move(speed);
    if(widget!=nullptr){
        widget->move(speed);
    }
    return 0;
}

void Game::GameObjects::NPC::newPointMove(sf::Vector2f p)
{
    if(flagClick)
        return;
    if(flagClick==false){
        flagClick = true;
        pointsMove.clear();
        pointsMove = Game::GameObjects::wayToThePoint(rectLocation.getPosition(),p,static_cast<int>(areaHit.getRadius()));
        flagClick = false;
    }
    clokFindTime.restart();
}

void Game::GameObjects::NPC::setWidget(Game::Widgets::Widget *w)
{
    widget = w;
}

bool Game::GameObjects::NPC::checkContactObject(long int i){
    auto object = Game::GameObjects::getDinamicObjects()->at(i);
    float len = sqrtf( powf(object->getPos().x - rectLocation.getPosition().x,2) + powf(object->getPos().y - rectLocation.getPosition().y,2));
    if(len < areaHit.getRadius()+object->getRadiusAreaHit())
    {
    }
//    if (object->getType()->nameRace!=type->nameRace&&clokFindTime.getElapsedTime()>sf::milliseconds(300))
//    {
//        if(len < type->Weapon1->range)
//        {
//            type->attackLeft(object->getPos());
//        }
//        newPointMove(object->getPos());
//    }
    return 0;
}

bool Game::GameObjects::NPC::animation(){
    return 0;
}
