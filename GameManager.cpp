#include "GameManager.h"
static Game::GameManager* gameManager = nullptr;

Game::GameManager::GameManager()
{
	if (gameManager == nullptr)
		gameManager = this;
	threadServerUpdate = new sf::Thread(&Game::GameManager::updateServer, this);
}

Game::GameManager* Game::GameManager::geThis()
{
	return gameManager;
}

void Game::GameManager::startGame()
{
	sf::Mutex mutex;
	srand(static_cast<unsigned int>(time(0)));
	sf::RenderWindow window(sf::VideoMode(WIDHT, HEIGHT), "MyGame");
	Game::GameObjects::setWindow(&window);
	Game::Interface* interface = new Game::Interface(&window);
	window.setVerticalSyncEnabled(true);
	//    window.setFramerateLimit(60);
	//    window.setMouseCursorVisible(true);
	sf::Texture textureMap;
	if (!textureMap.loadFromFile(static_cast<std::string>(DATA_PATH) + "sprite/map1000x1000.png"))
		std::cout << "Error";
	sf::Sprite map;
	map.setTexture(textureMap);
	map.setScale(3, 3);
	map.setTextureRect(sf::IntRect(0, 0, 1000, 1000));
	map.setOrigin(500, 500);
	map.setPosition(WIDHT / 2, HEIGHT / 2);

	start();

	//    if(Game::getServerTCP()==nullptr)
	//        new Game::ServerTCP();
	//    Game::getServerTCP()->playerUpdate = playerUpdate;


	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			interface->update(event);
			switch (event.type) {
			case sf::Event::Closed:
			{
				if (Game::getServerTCP() != nullptr)
					Game::getServerTCP()->serverStop();
				if (Game::getClientTCP() != nullptr)
					Game::getClientTCP()->disconnect();
				mutex.lock();
				delete Game::getServerTCP();
				delete Game::getClientTCP();
				Game::getInterface()->clearListWidget();
				Game::GameObjects::clearObjectToListMap();
				Game::GameObjects::clearStaticObject();
				window.close();
				mutex.unlock();
				return;
			}
			case sf::Event::KeyReleased:
				//////////////////////////////////////////////////////////////////////////////////////////
				switch (event.key.code)
				{
				case sf::Keyboard::Escape:
					if (pause != nullptr)
						pause();
					break;
				default:
					break;
				}
				/////////////////////////////////////////////////////////////////////////////////////////
				break;
			default:
				break;
			}
		}
		window.clear();
		window.draw(map);
		std::map<long int, Game::GameObjects::DinamicObject*>* list_objects = Game::GameObjects::getDinamicObjects();
		std::map<long int, sf::FloatRect>* location = Game::GameObjects::getArrayRectLocation();
		//        Game::getServerTCP()->send(doc);
		for (auto it = list_objects->begin(); it != list_objects->end(); it++) {

			//            (*list_objects)[it->first]->update(doc.get(std::to_string(it->first),minibson::document()));
			it->second->draw(&window);
		}
		player.draw(&window);

		for (auto it = Game::GameObjects::getListStaticObject()->begin(); it != Game::GameObjects::getListStaticObject()->end(); it++) {
			it->second->draw(&window);
		}
		Game::GameObjects::CheckContact();
		interface->draw();
		window.display();
	}
	return;
}




void Game::GameManager::updateServer()
{
	sf::Mutex mutex;
	std::map<long int, Game::GameObjects::DinamicObject*>* list_objects = Game::GameObjects::getDinamicObjects();
	std::map<long int, sf::FloatRect>* location = Game::GameObjects::getArrayRectLocation();
	minibson::document doc;
	for (auto it = list_objects->begin(); it != list_objects->end(); it++) {

		if (it->first == player.getIndexPlayer())
			continue;
		doc.set(std::to_string(it->first), it->second->update());

		mutex.lock();
		(*location)[it->first] = it->second->getRect();
		mutex.unlock();
	}
	for (auto it = list_objects->begin(); it != list_objects->end(); it++) {

		(*list_objects)[it->first]->update(doc.get(std::to_string(it->first), minibson::document()));
		//        it->second->draw(&window);
	}

	doc.set(std::to_string(player.getIndexPlayer()), player.update());
}

void Game::GameManager::updateClient()
{
	std::map<long int, Game::GameObjects::DinamicObject*>* list_objects = Game::GameObjects::getDinamicObjects();
	for (auto it = list_objects->begin(); it != list_objects->end(); it++) {

		//        (*list_objects)[it->first]->update(doc.get(std::to_string(it->first),minibson::document()));
		//        it->second->draw(&window);
	}
}
