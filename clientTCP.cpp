#include "game.h"

static Game::ClientTCP* client = nullptr;

Game::ClientTCP::ClientTCP(std::string l, sf::IpAddress a,unsigned short p)
{
    if(client==nullptr)
        client = this;
    login = l;
    address = a;
    port = p;
    threadClient = new sf::Thread(&ClientTCP::startClient,this);
}

Game::ClientTCP::~ClientTCP()
{
    client = nullptr;
}

void Game::ClientTCP::connect()
{
    threadClient->launch();
}

bool Game::ClientTCP::connectToServer()
{
    if(socket.connect(address, port) == sf::Socket::Done)
    {
        sf::Packet packet;
        sf::Socket::Status status = socket.receive(packet);
        if (status == sf::Socket::Status::Done)
        {
            std::string cmd;
            std::string data;
            if (packet >> cmd >> data) {
                if (cmd == "get" && data == "login")
                {
                    sf::Packet packet;
                    packet << login;
                    if (socket.send(packet) != sf::Socket::Status::Done) {
                        std::cout << "Error Send Client" << std::endl;
                        return false;
                    }
                    if(connectServer!=nullptr)
                        connectServer();
                    return true;
                }
            }
            else {
                socket.disconnect();
                std::cout << "Client wrong connect " << std::endl;
                return false;
            }
        }
    }
    return false;
}

void Game::ClientTCP::startClient()
{
    connectToServer();
    while (flagWork) {
        sf::Packet packet;
        sf::Socket::Status status = socket.receive(packet);
        if (status == sf::Socket::Status::Done)
        {
            mutex.lock();
            std::string cmd;
            std::string data;
            if(packet >> cmd >> data){
                if(cmd == "how are you")
                {
                    send("responce","I'm good");
                }
                std::cout << "Client::receiveMessage:  cmd = [" << cmd << "] data =[" << data << "]" << std::endl;
            }
            mutex.unlock();
        }
        else if (status == sf::Socket::Status::Error) {
            std::cout << "ClientTCP::receive Error" << std::endl;
        }
        else if(status == sf::Socket::Status::Disconnected)
        {
            std::cout << "ClientTCP::receive Disconect" << std::endl;
            disconnect();
            return;
        }
        else if(status == sf::Socket::Status::NotReady)
        {
            std::cout << "ClientTCP::receive NotReady" << std::endl;
        }
        else if(status == sf::Socket::Status::Partial)
        {
            std::cout << "ClientTCP::receive Partial" << std::endl;
        }
    }
    std::cout << "out Client receiveMessage " << std::endl;
}


void Game::ClientTCP::send(std::string cmd,std::string s)
{
    sf::Packet packet;
    packet << cmd << s;
    sf::Socket::Status status = socket.send(packet);
    if(status == sf::Socket::Status::Done){
        return;
    }
    else if (status == sf::Socket::Status::Error) {
        std::cout << "ClientTCP::send Error" << std::endl;
    }
    else if(status == sf::Socket::Status::Disconnected)
    {
        std::cout << "ClientTCP::send Disconect" << std::endl;
        disconnect();
    }
    else if(status == sf::Socket::Status::NotReady)
    {
        std::cout << "ClientTCP::send NotReady" << std::endl;
    }
    else if(status == sf::Socket::Status::Partial)
    {
        std::cout << "ClientTCP::send Partial" << std::endl;
    }
}

Game::ClientTCP* Game::getClientTCP(){
    return client;
}

////////////////////////////удаление косячит
void Game::ClientTCP::disconnect()
{
    flagWork = false;
    socket.disconnect();
    std::cout << "ClientTCP disconect" << std::endl;
}
