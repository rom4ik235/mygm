#ifndef GAME_H
#define GAME_H
#include "GameObject/gameobject.h"
#include "GameObject/StaticObject/staticobject.h"
#include "GameObject/Interface/Interface.h"
#include "GameObject/npc.h"
#include "GameObject/TypeObject/balllightning.h"
#include "GameObject/TypeObject/human.h"
#include "GameObject/StaticObject/bush.h"


namespace Game {


	void playerUpdate(std::string data);

	struct Socket
	{
		std::string login;
		sf::TcpSocket socket;
		sf::Thread* threadReseive;
		bool flagReseive = false;
	};

	class ServerTCP {
	public:
		ServerTCP(unsigned short port = 55001);
		void waitNewSocket();
		void disconnect(std::string login);
		void send(std::string login, std::string message);
		void send(Socket* socket, std::string message);
		void send(minibson::document doc);
		std::vector<std::string> getActiveConnect();
		void (*playerUpdate)(std::string data) = nullptr;
		void (*connectPlayer)(std::string login) = nullptr;
		void (*disconnectPlayer)(std::string login) = nullptr;
		void serverStop();


	private:
		void waitConnectNewSocket();
		void receiveMessage();
		sf::TcpListener listener;
		sf::Mutex mutex;
		bool flagWaitConnect = false;
		sf::Thread* threadWaitConnect;
		unsigned short port;
		std::vector<Socket*> sockets;
	};

	class ClientTCP {
	public:
		ClientTCP(std::string login, sf::IpAddress address = "192.168.10.35", unsigned short port = 55001);
		~ClientTCP();
		void connect();
		void disconnect();
		void send(std::string cmd, std::string s);
		void (*connectServer)(/*std::string login*/) = nullptr;
		void (*disconnectServer)(/*std::string login*/) = nullptr;

	private:
		void startClient();
		bool connectToServer();
		bool flagWork = true;
		sf::Mutex mutex;
		sf::Thread* threadClient;
		sf::IpAddress address;
		std::string login;
		unsigned short port;
		sf::TcpSocket socket;
	};


	//    class ServerUDP{
	//    public:
	//        ServerUDP(unsigned short port = 55001);
	//        void waitConnectNewSocket();
	//        void receiveMessage();
	//        void disconnect(std::string login);
	//        void send(std::string login,std::string message);
	//        void serverStop();
	//    private:
	//        bool flagWaitConnect;
	//        sf::Thread *threadServer;
	//        sf::Thread *threadReceive;
	//        sf::TcpListener listener;
	//        std::string login;
	//        unsigned short port;
	//        std::map<std::string,sf::TcpSocket*> socketList;
	//        sf::Socket::Status status;
	//    };

	//    class ClientUDP{
	//    public:
	//        ClientUDP(std::string login, sf::IpAddress address = "192.168.100.5", unsigned short port = 55001);
	//        //Client(std::string login, sf::IpAddress address = "192.168.10.35", unsigned short port = 55001);
	//        void connectToServer();
	//        void receiveMessage();
	//        void disconnect();
	//        void send(std::string s);
	//////        void re

	//    private:
	//        sf::Thread *threadClient;
	//        sf::Thread *threadReceive;
	//        sf::Thread *threadSend;
	//        sf::IpAddress address;
	//        std::string login;
	//        unsigned short port;
	//        sf::TcpSocket socket;
	//        sf::Socket::Status status;
	//    };


	ServerTCP* getServerTCP();
	ClientTCP* getClientTCP();
	//    ServerUDP* getServerUDP();
	//    ClientUDP* getClientUDP();

}



#endif // GAME_HPP
