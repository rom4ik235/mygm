#include "GameObject/Scene/Scene.h"

int main(void)
{
    Game::GameManager manger;
    manger.pause = Game::Scene::scenePause;
    manger.start = Game::Scene::sceneLogin;
    manger.startGame();
    return EXIT_SUCCESS;
}
